# Makefile
include offbeatLib/Make/options

PROJECT = offbeat

ifeq ($(findstring OPENFOAMFOUNDATION,$(VERSION_SPECIFIC_INC)), OPENFOAMFOUNDATION)
    VERSION = OPENFOAMFOUNDATION

    # Copy file.ESI
    $(shell cd offbeatLib/Make && ln -sf files.FOUNDATION files && cd ../..)
else
    VERSION = OPENFOAMESI

    # Copy file.FOUNDATION
    $(shell cd offbeatLib/Make && ln -sf files.ESI files && cd ../..)
endif

all :
	@echo Making all for project $(PROJECT)
	@echo Build type set to $(WM_COMPILE_OPTION)
	wmakeLnInclude SCIANTIX
	wmake -j SCIANTIX
	wmakeLnInclude offbeatLib
	wmake -j offbeatLib
	wmake -j offbeat
	wmake -j all utilities
	wmake -j functionObjects
# 	make -C doc/Doxygen all

clean :
	@echo Cleaning project $(PROJECT) 
	wclean offbeat	
	wclean offbeatLib
	wclean SCIANTIX
	wclean all utilities
	wclean functionObjects
#   make -C doc/Doxygen clean
