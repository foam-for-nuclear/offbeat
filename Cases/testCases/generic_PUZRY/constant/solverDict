/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version         2;
    format          ascii;
    class           dictionary;
    location        "constant";
    object          solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

thermalSolver       solidConduction;
mechanicsSolver     smallStrainIncrementalUpdated;
neutronicsSolver    fromLatestTime;
elementTransport    fromLatestTime;

materialProperties  byZone;
rheology            byMaterial;
heatSource          fromLatestTime;
burnup              fromLatestTime;
neutronics          fromLatestTime;
fastFlux            fromLatestTime;
corrosion           fromLatestTime;
gapGas              none;
fgr                 none;
sliceMapper         byMaterial;

globalOptions
{
    pinDirection    ( 0 0 1 );
    reactorType     ;
}

thermalSolverOptions
{
    heatFluxSummary off;
}

mechanicsSolverOptions
{
    forceSummary        off;  
    cylindricalStress   on;  
    RhieChowCorrection   true;
    RhieChowScaleFactor  1;
    multiMaterialCorrection
    {
        type                    uniform2D;
        defaultWeights          1;
    }

}

rheologyOptions
{
    thermalExpansion     on;
    modifiedPlaneStrain  off;
    planeStress          off;
    solvePressureEqn     off;
}

materials
{
    cladding
    {
        material                zircaloy;
        Tref                    Tref [ 0 0 0 1 0 ] 300;
        
        PoissonRatioModel       ZyConstant;

        failureModel            ZyOverstrainBISON;
        stopIfFailed            on;
        failureModelOptions
        {
            hoopStrainLimit         0.244897959;
            patchNames ( "outer" "inner" );
        }

        phaseTransitionModel ZyDynamic;

        nSlices         10;

        rheologyModel   misesPlasticCreep;
        rheologyModelOptions
        {
            plasticStrainVsYieldStress table
            (
                (0    600e6)
            );

            creepModel LimbackCreepModelLOCA;
            relax 0.1;

            primaryCreep     off;
            irradiationCreep off;

            NewtonRaphsonMethod on;
        }
    }
}


// ************************************************************************* //
