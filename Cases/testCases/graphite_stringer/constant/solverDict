/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version         2;
    format          ascii;
    class           dictionary;
    location        "constant";
    object          solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

thermalSolver       solidConduction;
mechanicsSolver     fromLatestTime;
neutronicsSolver    fromLatestTime;
elementTransport    fromLatestTime;

materialProperties  byZone;
rheology            byMaterial;

// With fromLatestTime the power Q will be read 
// directly from the 0/ folder
heatSource          fromLatestTime;

burnup              none;
neutronics          fromLatestTime;
fastFlux            fromLatestTime;
corrosion           fromLatestTime;
gapGas              none;
fgr                 none;
sliceMapper         none;

globalOptions
{
    pinDirection    ( 0 0 1 );
    reactorType     ;
}

thermalSolverOptions
{
    heatFluxSummary off;
}

materials
{
    stringer
    {
        material                constant;

        // Values from Zanetti et al (2015) 
        // At https://doi.org/10.1016/j.pnucene.2015.02.014
        rho         rho     [1 -3 0 0 0]    1874;
        Cp          Cp      [0 2 -2 -1 0]   1772;
        k           k       [1 1 3 -1 0]    53;

        
        // Dummy values given that the simulation is purely thermal

        emissivity emissivity [0 0 0 0 0]   0.808642;
        E           E       [1 -1 -2 0 0]   9e10;
        nu          nu      [0 0 0 0 0]     0.3;
        G           G       [1 -1 -2 0 0]   3.6241e+10;
        alpha       alpha   [0 0 0 0 0]     10e-6;

        // Reference temperature for thermal expansion
        Tref        Tref    [0 0 0 1 0]     300;

        rheologyModel   elasticity;
    }
}


// ************************************************************************* //
