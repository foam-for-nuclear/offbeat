#!/bin/sh
# Run from this directory
cd "${0%/*}" || exit 1

# Source tutorial run functions
. "$WM_PROJECT_DIR/bin/tools/RunFunctions"

################################ 1st simulation ###################################
runApplication -o blockMesh
runApplication -o changeDictionary
runApplication -o offbeat

CsProd1=$(grep -i "Production of Cs" log.offbeat | tail -n 1| cut -d" " -f4)
CsRele1=$(grep -i "Released Cs amount" log.offbeat | tail -n 1| cut -d" " -f4)
AgProd1=$(grep -i "Production of Ag" log.offbeat | tail -n 1| cut -d" " -f4)
AgRele1=$(grep -i "Released Ag amount" log.offbeat | tail -n 1| cut -d" " -f4)


case_dir1='./4.32e+07'
case_dir2='./4.392e+07'
new_value1="1873.15"
new_value2="2073.15"

################################ 2nd simulation ###################################
# Update the internalField value in 4.32e+07/T file
sed -i "s/internalField   uniform 1273.15;/internalField   uniform $new_value1;/g" "$case_dir1/T"
# Update the value in the boundaryField section for the "outer" patch
sed -i "s/value           uniform 1273.15;/value           uniform $new_value1;/g" "$case_dir1/T"
# change end time and timeStep
controlDict='./system/controlDict'
runApplication -o foamDictionary $controlDict -entry endTime -set 4.392e7
runApplication -o foamDictionary $controlDict -entry deltaT -set 3600

runApplication -o offbeat

CsProd2=$(grep -i "Production of Cs" log.offbeat | tail -n 1| cut -d" " -f4)
CsRele2=$(grep -i "Released Cs amount" log.offbeat | tail -n 1| cut -d" " -f4)
AgProd2=$(grep -i "Production of Ag" log.offbeat | tail -n 1| cut -d" " -f4)
AgRele2=$(grep -i "Released Ag amount" log.offbeat | tail -n 1| cut -d" " -f4)

################################ 3rd simulation ###################################
# Update the internalField value in 4.32e+07/T file
sed -i "s/internalField   uniform 1873.15;/internalField   uniform $new_value2;/g" "$case_dir2/T"
# Update the value in the boundaryField section for the "outer" patch
sed -i "s/value           uniform 1873.15;/value           uniform $new_value2;/g" "$case_dir2/T"
# change end time and timeStep
controlDict='./system/controlDict'
runApplication -o foamDictionary $controlDict -entry endTime -set 4.464e7

runApplication -o offbeat

CsProd3=$(grep -i "Production of Cs" log.offbeat | tail -n 1| cut -d" " -f4)
CsRele3=$(grep -i "Released Cs amount" log.offbeat | tail -n 1| cut -d" " -f4)
AgProd3=$(grep -i "Production of Ag" log.offbeat | tail -n 1| cut -d" " -f4)
AgRele3=$(grep -i "Released Ag amount" log.offbeat | tail -n 1| cut -d" " -f4)

############################### Postprecessor ########################################
#
CsProd=$(awk -v var1="$CsProd1" \
             -v var2="$CsProd2" \
             -v var3="$CsProd3" \
             'BEGIN {print var1+var2+var3}')

CsRele=$(awk -v var1="$CsRele1" \
             -v var2="$CsRele2" \
             -v var3="$CsRele3" \
             'BEGIN {print var1+var2+var3}')

AgProd=$(awk -v var1="$AgProd1" \
             -v var2="$AgProd2" \
             -v var3="$AgProd3" \
             'BEGIN {print var1+var2+var3}')

AgRele=$(awk -v var1="$AgRele1" \
             -v var2="$AgRele2" \
             -v var3="$AgRele3" \
             'BEGIN {print var1+var2+var3}')

fractionReleaseCs=$(awk -v var1="$CsRele" \
                        -v var2="$CsProd" \
                        'BEGIN {print var1/var2}')

fractionReleaseAg=$(awk -v var1="$AgRele" \
                        -v var2="$AgProd" \
                        'BEGIN {print var1/var2}')


CsExpected=0.223638
AgExpected=0.915856

#

echo "Released Cs fraction: $fractionReleaseCs" >> results
echo "Codes range: [0.21, 0.24]" >> results
echo "Expected value: $CsExpected" >> results

echo "Released Ag fraction: $fractionReleaseAg" >> results
echo "Codes range: [0.92, 0.98]" >> results
echo "Expected value: $AgExpected" >> results
#
relErrorCs=$(awk -v var1="$fractionReleaseCs" \
                 -v var2="$CsExpected" \
                 'BEGIN {print 100 * (var1 - var2) / var2}')

relErrorKr=$(awk -v var1="$fractionReleaseAg" \
                 -v var2="$AgExpected" \
                 'BEGIN {print 100 * (var1 - var2) / var2}')



echo "Relative change for Cs: $relErrorCs%" >> results
echo "Relative change for Ag: $relErrorKr%" >> results


runApplication -o foamDictionary $controlDict -entry endTime -set 4.32e7
runApplication -o foamDictionary $controlDict -entry deltaT -set 86400
