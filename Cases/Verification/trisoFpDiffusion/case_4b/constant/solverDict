/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           fromLatestTime;
mechanicsSolver         fromLatestTime;
neutronicsSolver        fromLatestTime;
elementTransport        byList;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              timeDependentVhgr;
burnup                  fromPower;
fastFlux                timeDependentAxialProfile;
corrosion               fromLatestTime;
gapGas                  none;
fgr                     none;
sliceMapper             autoAxialSlices;
corrosion               fromLatestTime;

elementTransportOptions
{
  solvers
  (
      FpDiffusion
  );
  FpDiffusionOptions
  {
    outerPatches   outer;
    intialFpConcentration
    {
      Cs       0;
      Ag       0;
    }
  }
}

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

heatSourceOptions
{
    timePoints  (0.0    1e9);
    vhgr        (1785990209 1785990209);
    //fission_rate = Bu*Nf/t = 0.1*(10810*6.0221409e23/0.27)/4.32e7 = 5.581219404e19 f/m³/s
    // Q = fission_rate * energy_per_fission
    // = 5.581219404e19 * 3.2e-11 J/f
    materials ( Kernel );
}


rheologyOptions
{
    thermalExpansion off;
}

fastFluxOptions
{
  //2e25 n/m² at 4.32e7  (E > 0.1MeV)
  //2e25/4.32e7/1.1=4.208754209e17 n/m2/s
    timePoints  ( 0     1e9);

    fastFlux    (4.208754209e13    4.208754209e13); // n/cm2/s
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    materials (Kernel Buffer IPyC SiC OPyC);
}

mechanicsSolverOptions
{
    forceSummary        off;
    cylindricalStress   on;

    multiMaterialCorrection
    {
        type                    uniform;
        defaultWeights          1;
    }
}


materials
{
  Kernel
  {
    material UO2;
    Tref                        Tref [ 0 0 0 1 0 ] 298.15;

    densificationModel          none;
    swellingModel               none;
    relocationModel             none;
    emissivityModel             constant;
    enrichment                  0.08;
    rGrain                      6e-6;
    theoreticalDensity          10960;
    densityFraction             0.9863138686;
    dishFraction                0.0;
    isotropicCracking           off;
    nCracksMax                  12;

    emissivity  emissivity      [0 0 0 0 0]     0.0;

    rheologyModel               elasticity;

    initialCsConcentration      1.0;

  }

  Buffer
  {
    material buffer;

    densityModel                constant;
    conductivityModel           constant;
    heatCapacityModel           constant;
    emissivityModel             constant;
    YoungModulusModel           constant;
    PoissonRatioModel           constant;
    swellingModel               none;

    rho         rho     [1 -3 0 0 0]    1880.0;
    Cp          Cp      [0 2 -2 -1 0]   720.0;
    k           k       [1 1 3 -1 0]    4.0;
    emissivity emissivity [0 0 0 0 0]   1.0;
    E           E       [1 -1 -2 0 0]   3.96e10;
    nu          nu      [0 0 0 0 0]     0.33;
    alpha       alpha   [0 0 0 0 0]     5.5e-6;
    Tref        Tref    [0 0 0 1 0]     1608;

    fissionProductsTransport
    {
      fissionProductsName  ("Cs" "Ag");
      Cs
      {
        d1        1e-8;
      }
      Ag
      {
        d1        1e-8;
      }
    }

    rheologyModel               elasticity;
  }

    "IPyC|OPyC"
    {
      material PyC;

      densityModel                constant;
      conductivityModel           constant;
      heatCapacityModel           constant;
      emissivityModel             constant;
      YoungModulusModel           constant;
      PoissonRatioModel           constant;
      swellingModel               none;

      rho         rho     [1 -3 0 0 0]    1880.0;
      Cp          Cp      [0 2 -2 -1 0]   720.0;
      k           k       [1 1 3 -1 0]    4.0;
      emissivity emissivity [0 0 0 0 0]   1.0;
      E           E       [1 -1 -2 0 0]   3.96e10;
      nu          nu      [0 0 0 0 0]     0.33;
      alpha       alpha   [0 0 0 0 0]     5.5e-6;
      Tref        Tref    [0 0 0 1 0]     1608;

      rheologyModel               elasticity;
    }

    SiC
    {
      material SiC;

      densityModel                constant;
      conductivityModel           constant;
      heatCapacityModel           constant;
      emissivityModel             constant;
      YoungModulusModel           constant;
      PoissonRatioModel           constant;

      rho         rho     [1 -3 0 0 0]    3200.0;
      Cp          Cp      [0 2 -2 -1 0]   620.0;
      k           k       [1 1 3 -1 0]    13.9;
      emissivity emissivity [0 0 0 0 0]   0.0;
      E           E       [1 -1 -2 0 0]   3.7e11;
      nu          nu      [0 0 0 0 0]     0.13;
      alpha       alpha   [0 0 0 0 0]     4.9e-6;
      Tref        Tref    [0 0 0 1 0]     1608;

      rheologyModel               elasticity;
    }
}

// ************************************************************************* //
