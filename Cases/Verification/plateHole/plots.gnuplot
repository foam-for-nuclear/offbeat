set terminal png
set output 'sigma.png'
set title "Stress Components"
set xlabel "y (m)"
set ylabel "Stress (MPa)"
plot [0.5:4] [0:] "postProcessing/xyGraph/1/symmetry_sigma_epsilon.xy" using ($1):($2/1e6) title "xx" with lines, 8/7*(1+(0.125/(x**2))+(0.09375/(x**4))) title "analytical"

set output 'epsilon.png'
set title "Strain Components"
set xlabel "y (m)"
set ylabel "Strain x 10^{-6} (-)"
plot "postProcessing/xyGraph/1/symmetry_sigma_epsilon.xy" using ($1):($8*1e6) title "xx" with lines
