/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           solidConduction;
mechanicsSolver         fromLatestTime;
neutronicsSolver        fromLatestTime;
elementTransport        fromLatestTime;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              timeDependentLhgr;
burnup                  fromLatestTime;
fastFlux                fromLatestTime;
gapGas                  FRAPCON;
fgr                     none;
sliceMapper             byMaterial;
corrosion               byPatch;

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

rheologyOptions
{
    modifiedPlaneStrain  off;
}

gapGasOptions
{
    gapPatches ( fuelOuter cladInner );
    holePatches ();
    topFuelPatches    ( fuelTop);
    bottomFuelPatches ( fuelBottom);

    gapVolumeOffset 0.0;
    gasReserveVolume 0.0;
    gasReserveTemperature 290;
}

heatSourceOptions
{
    timePoints  ( 0   1.E+10 );
    lhgr        ( 100e2 100e2);
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    radialProfile
    {
        type    flat;
    }

    materials ( fuel );
}

corrosionOptions
{
    updateMesh false;
    patches
    {
        cladOuter zircaloyOuterCorrosion;
    }
}

materials
{
    fuel
    {
        material                    UO2;
        Tref                        Tref [ 0 0 0 1 0 ] 293;

        densificationModel          UO2FRAPCON;
        swellingModel               UO2FRAPCON;
        relocationModel             UO2FRAPCON;

        enrichment                  0.045;
        rGrain                      2.8e-05;
        GdContent                   0.0;
        theoreticalDensity          10960;
        densityFraction             0.95;
        dishFraction                0.0;

        resinteringDensityChange    0.3;
        GapCold                     0.075e-3;
        DiamCold                    9.0e-3;
        recoveryFraction            0.5;
        outerPatch                  "fuelOuter";

        isotropicCracking           on;
        nCracksMax                  12;

        nSlices                     1;

        rheologyModel               elasticity;
    }

    cladding
    {
        material                zircaloy;
        Tref                    Tref [ 0 0 0 1 0 ] 293;
        conductivityModel       constant;
        k                       k    [ 1 1 -3 -1 0] 16;

        nSlices                 1;

        rheologyModel   misesPlasticCreep;
        rheologyModelOptions
        {
            plasticStrainVsYieldStress table
            (
                (0    250e6)
            );

            creepModel LimbackCreepModel;
            relax 1.0;
        }
    }
}

// ************************************************************************* //
