/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::unilateralContactFvPatchVectorField

Description
    The `unilateralContact` boundary condition enforces a contact-like behavior 
    on the patch. In the parallel direction, sliding is always allowed, with no 
    shear forces applied. 

    In the normal direction, the patch displacement is restricted from being 
    positive (relative to the patch normal). If a positive displacement is attempted, 
    a fictitious spring force is applied to counteract it, driving the displacement 
    towards zero. The user can specify the spring modulus, where a large value 
    minimizes the positive displacement. A relaxation factor can be used to aid 
    convergence. The spring constraint is enforced in an implicit manner.

    When the patch detaches inward (negative displacement relative to the patch normal), 
    zero pressure is applied.

    TODO:
    
    - Add verification cases.
    - Check implementation and convergence behavior. Is spring modulus best choice?
    - Allow non-zero pressure when detaching inward.

________________________________________________________________________________

\par Options
    The `unilateralContact` fvPatchField can be selected in the patch subdictionary 
    inside the `boundaryField` subdictionary of the displacement field.

    Parameters in the patch subdictionary for `unilateralContact`:

    - <b>`springModulus`</b> - Spring modulus in N/m used to control the spring force. 
      A large value minimizes positive displacement. 
    - <b>`relax`</b> - Relaxation factor for the spring contribution. Helps improve 
      convergence. **Default: `0.1`.**

________________________________________________________________________________

Usage
    To apply the `unilateralContact` boundary condition to a given patch, the 
    following example can be used as a template:

    \verbatim
        cladOuter
        {
            type            unilateralContact;

            // Relaxation factor for spring contribution
            relax           0.1;

            // Spring modulus in N/m
            springModulus   1e8;
        }
    \endverbatim

SourceFiles
    unilateralContactFvPatchVectorField.C

\mainauthor
    Philip Cardiff, UCD/UT. All rights reserved. Based on velocityDisplacement by Aleksandar Karac


\*---------------------------------------------------------------------------*/

#ifndef unilateralContactFvPatchVectorField_H
#define unilateralContactFvPatchVectorField_H

#include "fvPatchFields.H"
#include "solidDirectionMixedFvPatchVectorField.H"
#include "Switch.H"
#include "Table.H"

#ifdef OPENFOAMESI
    #include "Function1.H"
#endif   

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
        Class unilateralContactFvPatchVectorField Declaration
\*---------------------------------------------------------------------------*/

class unilateralContactFvPatchVectorField
:
    public solidDirectionMixedFvPatchVectorField
{
protected:
    
    // Protected data
        
        // Surface pressure
        scalarField pressure_;
        
        // relaxation factor for update
        scalar relax_;

        //- Spring modulus
        scalar fixedSpringModulus_; 

        //- Patch weights for spring-dashpot system
        scalarField w_;   


public:

    //- Runtime type information
    TypeName("unilateralContact");


    // Constructors

        //- Construct from patch and internal field
        unilateralContactFvPatchVectorField
        (
            const fvPatch&,
            const DimensionedField<vector, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        unilateralContactFvPatchVectorField
        (
            const fvPatch&,
            const DimensionedField<vector, volMesh>&,
            const dictionary&,
            const bool valueRequired = true
        );

        //- Construct by mapping given
        // unilateralContactFvPatchVectorField
        //  onto a new patch
        unilateralContactFvPatchVectorField
        (
            const unilateralContactFvPatchVectorField&,
            const fvPatch&,
            const DimensionedField<vector, volMesh>&,
            const fvPatchFieldMapper&
        );

        //- Construct as copy setting internal field reference
        unilateralContactFvPatchVectorField
        (
            const unilateralContactFvPatchVectorField&,
            const DimensionedField<vector, volMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual tmp<fvPatchField<vector> > clone
        (
            const DimensionedField<vector, volMesh>& iF
        ) const
        {
            return tmp<fvPatchField<vector> >
            (
                new unilateralContactFvPatchVectorField(*this, iF)
            );
        }

    // Destructor

        virtual ~unilateralContactFvPatchVectorField()
        {}


    // Member functions

        // Mapping functions

            //- Map (and resize as needed) from self given a mapping object
            virtual void autoMap
            (
                const fvPatchFieldMapper&
            );

            //- Reverse map the given fvPatchField onto this fvPatchField
            virtual void rmap
            (
                const fvPatchField<vector>&,
                const labelList&
            );


        // Evaluation functions

            //- Update the coefficients associated with the patch field
            virtual void updateCoeffs();


        //- Write
        virtual void write(Ostream&) const;


    // Member operators

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
