import matplotlib.pyplot as plt
import numpy			 as np
from pathlib import Path
from rdp 	 import rdp
import sys
import re
import os

# = FUNCTIONS DEFINITION =======================================================

# - Reads an OpenFOAM list and transforms it into a numpy array
def OpenFoamList2Array(list, filePath):
	listName = str(list)
	file     = Path(filePath)
	
	s = ''
	last = ''
	first = ''

	with open(file, 'r') as f:
		copy = False
		for line in f:
			for word in line.split():
				if re.search(listName,word):
					copy = True
					first = word.replace(listName,'')+'\n'
				elif re.search(r';',word):
					copy = False
					last = word.translate({ ord(c): None for c in ');' })
				elif copy:
					s+=str(word)+'\n'

	res=(first+s+last).translate({ ord(c): None for c in '();' })
	return np.fromstring(res, sep='\n')

# - Transforms an array in a OpenFOAM list
def Array2OpenFoamList(list_, listName, oldFilePath):
	
	newFilePath = str(oldFilePath)+'_new'
	
	with open(newFilePath, 'a') as f:
		f.write(listName+'\n(')
		for i in range(0,len(list_)):
			f.write('\n{:f}'.format(list_[i]))
		f.write('\n);\n\n')

# - Writes the new axial profile in a OpenFOAM format
def OutputAxialProfile(axProfRaw, timeRaw, timeNew, oldFilePath):
	
	# Find indeces of the New list wrt to Raw list
	idx = np.where(np.in1d(timeRaw, timeNew))[0]
	
	# Get number of axial points
	nAxialPoints = int(len(axProfRaw)/len(timeRaw))

	# Axial profile 2d array
	axProfRaw = axProfRaw.reshape(-1,nAxialPoints) 
	axProfNew = axProfRaw[idx,:] 

	# Output file
	newFilePath = str(oldFilePath)+'_new'
	with open(newFilePath, 'a') as f:
		f.write('data'+'\n(')
		for i in range(0, len(timeNew)):
			f.write("\n\t(")
			for j in range(0, len(axProfNew[i,:])):
				f.write("{:f} ".format(axProfNew[i,j]))
			f.write(")")
		f.write(");\n\n")
	

# = MAIN =======================================================================

#-------------------------------------------------------------------------------
#  Reading files
#-------------------------------------------------------------------------------

# Name of the input file
inputFile = Path('historySimplifierDict')

# Check if input dictionary is present
try:
	inputFile.resolve(strict=True)
except FileNotFoundError:
	print('\nInput dictionary not found!' )
	print('Please check that the input file is located in the same folder')
	print('where the script is being executed.\n')
	sys.exit()

# Read input dictionary
inputDict = eval(open(inputFile).read())

# Read paths
lhgrPath = Path(inputDict['lhgrPath'])
timePath = Path(inputDict['timePointsPath'])

# Store data in arrays
lhgrRaw = OpenFoamList2Array('lhgr'		 ,lhgrPath)
timeRaw = OpenFoamList2Array('timePoints',timePath)

#-------------------------------------------------------------------------------
#  Running algorithm
#-------------------------------------------------------------------------------

# Running RDP algorithm
epsilon = inputDict['epsilon']
points = np.column_stack([timeRaw,lhgrRaw])
mask = rdp(points, epsilon=epsilon, return_mask=True)

# Store results in arrays
lhgrNew = points[mask,1]
timeNew = points[mask,0]

#-------------------------------------------------------------------------------
#  Local refinement
#-------------------------------------------------------------------------------
if ( inputDict['refinedInterval'] ):

	for i in range(0, len(inputDict['tStart'] )):

		s = inputDict['tStart'][i]
		e = inputDict['tEnd'][i]
		
		# Get indexes of the elements between tStart and tEnd
		idx = np.where( ((timeNew >= s) & (timeNew <= e)) )
		
		# Remove those elements from mother list
		timeNew = np.delete(timeNew, idx)
		lhgrNew = np.delete(lhgrNew, idx)

		# Create subLists 
		del idx
		idx = np.where( ((timeRaw >= s) & (timeRaw <= e)) )
		subLhgrRaw = lhgrRaw[idx]
		subTimeRaw = timeRaw[idx]

		# Append resulting subLists to mother list
		lhgrNew = np.concatenate((lhgrNew, subLhgrRaw))
		timeNew = np.concatenate((timeNew, subTimeRaw))

	# Sort the mother lists according to time
	sortIdx = timeNew.argsort()
	timeNew = timeNew[sortIdx] 
	lhgrNew = lhgrNew[sortIdx]

#-------------------------------------------------------------------------------
#  Print info
#-------------------------------------------------------------------------------

print('\nRDP algorithm results : ')
print('\tLength raw power history = ', len(lhgrRaw))
print('\tLength new power history = ', len(lhgrNew))
print('\n')

#-------------------------------------------------------------------------------
#  Files output
#-------------------------------------------------------------------------------

# Remove _new files if existent
if ( os.path.exists(Path(str(lhgrPath)+'_new')) ):
	os.remove(Path(str(lhgrPath)+'_new'))

if ( os.path.exists(Path(str(timePath)+'_new')) ):
	os.remove(Path(str(timePath)+'_new'))

# Output axial profile ( only if present )
if ( 'axialProfilePath' in inputDict ):
	
	# Read path from dictionary
	axProfPath = Path(inputDict['axialProfilePath'])
	
	# Remove _new file if existent
	if ( os.path.exists(Path(str(axProfPath)+'_new')) ):
		os.remove(Path(str(axProfPath)+'_new'))

	# Extract axial profile as a 1d array
	axProfRaw = OpenFoamList2Array('data', axProfPath)

	OutputAxialProfile(axProfRaw, timeRaw, timeNew, axProfPath)

# Output lhgr and time arrays
Array2OpenFoamList(lhgrNew, 'lhgr'		, lhgrPath)
Array2OpenFoamList(timeNew, 'timePoints', timePath)

#-------------------------------------------------------------------------------
#  Plotting
#-------------------------------------------------------------------------------
if ( inputDict['plotResult'] ):
	plt.figure()
	plt.plot(timeNew, lhgrNew, color='r', marker='o', label='after')
	plt.plot(timeRaw, lhgrRaw, color='k', label='before')
	plt.legend()
	plt.ylabel('Linear Heat Generation Rate (W/m)')
	plt.xlabel('Time (s)')
	plt.show()



