%==========================================
%
% - UO2 LWR Pellet for XS generation
%
%==========================================

set title "UO2pellet"

% Set XS library:
set acelib "/media/storage/shared/nuclearDataLibraries/endfb7/sss_endfb7u.xsdata"
set declib "/media/storage/shared/nuclearDataLibraries/endfb7/sss_endfb7.dec"
set nfylib "/media/storage/shared/nuclearDataLibraries/endfb7/sss_endfb7.nfy"

% --- Define thermal scattering libraries associated with hydrogen in light water
%     As there are no readymade thermal scattering libraries for 600 K
%     We will tell Serpent to interpolate using two bounding libraries:
%     -lwj3.11t (H-1 in light water at 574 K)
%     -lwj3.13t (H-1 in light water at 624 K)
%     See also: http://montecarlo.vtt.fi/download/SSS_THERMAL.pdf
therm lwtr 600 lwj3.11t lwj3.13t

%==========================================
% - Materials
%==========================================

% --- Fuel material
mat fuel -10.4215 rgb 255 255 150 burn 1

% --- Water 
mat water -0.6604292670741628  tmp 600 moder lwtr 1001 rgb 200 200 255
    8016.06c       -0.888515712289727
    1001.06c        -0.11106446403621588
    5010.06c       -8.354491113732231e-05
    5011.06c       -0.0003362787629195737

% --- Gap gas : Helium
mat gapGas -1E-4 rgb 230 230 230
    2004.09c     1.0

% --- Cladding material Zircaloy-4 [from PNNL-15870, Rev. 1]
mat cladding -6.55000E+00 rgb 150 150 150
     8016.06c  -1.19276E-06
    24050.06c  -4.16117E-05
    24052.06c  -8.34483E-04
    24053.06c  -9.64457E-05
    24054.06c  -2.44600E-05
    26054.06c  -1.12572E-04
    26056.06c  -1.83252E-06
    26057.06c  -4.30778E-05
    26058.06c  -5.83334E-06
    40090.06c  -4.97862E-01
    40091.06c  -1.09780E-01
    40092.06c  -1.69646E-01
    40094.06c  -1.75665E-01
    40096.06c  -2.89068E-02
    50112.06c  -1.27604E-04
    50114.06c  -8.83732E-05
    50115.06c  -4.59255E-05
    50116.06c  -1.98105E-06
    50117.06c  -1.05543E-06
    50118.06c  -3.35688E-06
    50119.06c  -1.20069E-06
    50120.06c  -4.59220E-06
    50122.06c  -6.63497E-04
    50124.06c  -8.43355E-04

%==========================================
% - Surfaces
%==========================================

% --- Square surface centered at (x,y) = (0,0)
surf s1 sqc 0.0 0.0 0.748

% --- Bounding surfaces for universe 0
surf s2 cyl 0.0 0.0 0.4650 % fuel outer radius
surf s3 cyl 0.0 0.0 0.4840 % clad inner radius
surf s4 cyl 0.0 0.0 0.5375 % clad outer radius

%==========================================
% - Cells
%==========================================

% --- Create universe 0
cell c1 0 fuel       -s2
cell c2 0 gapGas     -s3 s2 
cell c3 0 cladding   -s4 s3 
cell c4 0 water      -s1 s4 
cell c5 0 outside     s1

% --- Create u=1000 (for XS generation)
cell c6 1000 fuel    -s2

%==========================================
% - Run Parameters
%==========================================

% --- x, y, z BC (1 = black, 2 = reflective, 3 = periodic)
set bc 2

% --- n. per cycle - n. active cycles - n. inactive cycles 
set pop 5000 60 20

% --- Set energy groups for constant generation
%set nfg 2 5
ene oneGroup 1 1E-11 20.0
set nfg oneGroup

% Group constant generation
set gcu 1000

% Microscopic
set mdep 0 1 1 fuel
    % --- Fission:
    922350 18 % U235
    922360 18 % U236
    922380 18 % U238
    932370 18 % Np237
    942380 18 % Pu238
    942390 18 % Pu239
    942400 18 % Pu240
    942410 18 % Pu241
    942420 18 % Pu242
    952410 18 % Am241
    952430 18 % Am243
    962420 18 % Cm242
    962430 18 % Cm243
    962440 18 % Cm244
    % --- Capture:
    922350 101 % U235
    922360 101 % U236
    922380 101 % U238
    932370 101 % Np237
    942380 101 % Pu238
    942390 101 % Pu239
    942400 101 % Pu240
    942410 101 % Pu241
    942420 101 % Pu242
    952410 101 % Am241
    952430 101 % Am243
    962420 101 % Cm242
    962430 101 % Cm243
    962440 101 % Cm244

%==========================================
% - Depletion steps
%==========================================

% --- Power density kW/gU:
set powdens 40.0e-3

% --- Cumulative burnup steps in MWd/kgHM:
dep butot
    40

%==========================================
% - Plots
%==========================================
plot 3  200  200