# **Theory Manual**

This is a placeholder for a more detailed theory manual that will be compiled in the future.
For now we can point to the basic theoretical description for OFFBEAT that is currently provided in the PhD thesis of Scolaro [@ScolaroThesis].

![Image title](https://upload.wikimedia.org/wikipedia/commons/2/20/UnderCon_icon.svg)
**This page is a work in progress!!!**