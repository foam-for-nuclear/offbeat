# **OFFBEAT Documentation**

OpenFOAM Fuel BEhavior Analysis Tool (OFFBEAT) is a multi-dimensional (1D, 2D, 3D) finite-volume nuclear fuel performance code based on the [OpenFOAM® C++ library](https://openfoam.org/). The first version of OFFBEAT is essentially the product of the research of Scolaro[@ScolaroThesis] [@ScolaroOffbeatNED]. Building on the works of Jasak and Weller[@JasakWellerLinearElasticity], Tuković[@TukovicFluidSolid], Cardiff[@Cardiff30] and Clifford[@CliffordLocalPeakingBWR], OFFBEAT is developed according to a cell-centered finite-volume framework for both small strain and finite strain solid mechanics. This is combined with a framework for thermal analysis and with numerical developments concerning the treatment of the gap heat transfer and contact, based on a mapping algorithm that allows the use of independent non-conformal meshes for fuel and cladding. The code considers the temperature and burnup dependence of the material properties, and it can model fuel densification, relocation, swelling, growth, fission gas release, creep, plasticity, and other relevant fuel behavior phenomena.

OFFBEAT is a joint development by the [Laboratory of Reactor Safety (LRS)](https://www.epfl.ch/labs/lrs) at École Polytechnique Fédérale de Lausanne (EPFL) and [Laboratory for Reactor Physics and Thermal-Hydraulics (LRT)](https://www.psi.ch/en/lrt) at the Paul Scherrer Institut (PSI).

--- 

## **Contents**

- [Release Notes](release_notes/index.md)
- [Installation](installation.md)
- [Theory Manual](theory_manual/index.md)
- [User's Guide](user_guide/index.md)
- [Tools](tools/index.md)
- [Tutorials](tutorials/index.md)
- [Publications](publications/index.md)
- [References](bibliography.md)