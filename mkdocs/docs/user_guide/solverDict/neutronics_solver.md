# <b>Neutronics Solver</b>

The <code><b>neutronicsSubSolver</b></code> class is designed to compute the neutron flux distribution within fuel pellets. While neutron flux calculations are not typically performed within fuel performance codes, especially using dedicated neutronics solvers, they can become essential when radial and symmetric profiles cannot be assumed, as often done in classical 1.5D simulations. In such cases, a full 3D neutron flux solution may be required to accurately capture spatial variations. Potentially, the neutorn flux could be an input for depletion calculations if a full depletion module is introduced in OFFBEAT in the future.

The neutronics solver can be selected with the <code><b>neutronicsSolver</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code> dictionary, located in the <code><b>constant</b></code>  folder). 

## <b>Classes</b>

Currently OFFBEAT supports the following neutronics solvers:

- [none](../../classes/physicsSubSolvers/neutronicsSubSolver/neutronicsSubSolver.md) - Deactivates the neutronics solution (i.e. no equation is solved). No additional field is created. **This is the default choice in OFFBEAT if the user does not specify the `neutronicsSolver` model in the `solverDict`**;
- [diffusion](../../classes/physicsSubSolvers/neutronicsSubSolver/diffusionSolver.md) - it solves the one-group neutron diffusion equation to obtain the thermal intra-pellet thermal neutron flux profile.