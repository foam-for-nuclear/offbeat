# **Mechanics Solver**

The `mechanicsSubSolver` class in OFFBEAT is responsible for solving the mechanics of materials, specifically the linear momentum balance equation. This equation governs the behavior of mechanical forces and is expressed as:

$$
\begin{aligned}
\int_V \frac{\partial^2 \rho {\bf D}}{\partial t^2} dV = \oint_S{\bf n} \cdot {\bf \sigma} dS + \int_V \rho {\bf b} dV
\end{aligned}
$$

where:

* \( V \) represents the body volume.
* \( S \) is the surface boundary of the volume \( V \).
* \( {\bf n} \) is the outward-pointing unit vector on the surface \( S \).
* \( \rho \) is the material density.
* \( {\bf D} \) is the displacement vector.
* \( {\bf \sigma} \) is the Cauchy stress tensor.
* \( {\bf b} \) is any generic body force per unit mass.

This equation states that the sum of all forces applied to the body equals the rate of change of its total linear momentum (or inertia).

The primary field in the mechanics solver is the **displacement**, represented by `D` for total displacement and `DD` for incremental displacement solvers. Both fields are expressed in `m`. The corresponding displacement field file must be present at the start of the simulation to set both the internal field and boundary conditions accordingly.

The mechanics solver <u>must</u> be selected with the `mechanicsSolver` keyword in the main OFFBEAT configuration dictionary, `solverDict`, located in the `constant` folder.

???+ note

	The `mechanicsSolver` often requires the use of the [`multiMaterialInterface`](multi_material_correction.md) correction to achieve more accurate results. Check the respective documentation.

## **Classes**

Currently, OFFBEAT supports the following mechanics solvers:

- [fromLatestTime](../../classes/physicsSubSolvers/mechanicsSubSolver/mechanicsSubSolver.md) - Deactivates the mechanics solution (i.e., no equation is solved). The displacement `D` is read from the initial time folder.  
- [smallStrain](../../classes/physicsSubSolvers/mechanicsSubSolver/smallStrain.md) - Solves the momentum continuity equation using a small-strain approximation. This solver calculates the total displacement `D` without updating the computational mesh.
- [smallStrainIncrementalUpdated](../../classes/physicsSubSolvers/mechanicsSubSolver/smallStrainIncUpdated.md) - Solves the momentum continuity equation with a small-strain approximation and updates the computational mesh. The solver calculates incremental displacement `DD`.
- [largeStrainTotLag](../../classes/physicsSubSolvers/mechanicsSubSolver/largeStrainTotLag.md) - Solves the momentum continuity equation with a large-strain approach but does not update the computational mesh. It calculates the total displacement `D`.
- [largeStrainUpdLag](../../classes/physicsSubSolvers/mechanicsSubSolver/largeStrainUpdLag.md) - Solves the momentum continuity equation with a large-strain approach, updating the computational mesh. The solver calculates incremental displacement `DD`.

???+ warning

	When using an incremental or updated solver, the boundary and initial conditions **must** be set for ``DD`` and not for `D`. Indeed one can neglect defining `D` in the initial time step folder, as it will be calculated by the simulation. Also, one needs to define the linear solver and related residual thresholds for `DD` in the `fvOption` file in the `system` folder.