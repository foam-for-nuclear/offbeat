# <b>Fast Flux and Fast Fluence</b>

The <code><b>fastFlux</b></code> class is used to enable the modeling of fast flux and fast fluence in OFFBEAT.  
The two fields are by default named <code><b>fastFlux</b></code> and <code><b>fastFluence</b></code>, and are in <code>n/cm<sup>2</sup>/s</code> and <code>n/cm<sup>2</sup></code>, respectively.

The fast flux model can be selected with the <code><b>fastFlux</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code> dictionary, located in the <code><b>constant</b></code> folder).

## <b>Classes</b>

Currently, OFFBEAT supports the following fast flux models:

- [none](../../classes/fastFlux/fastFlux.md) - it neglects fast flux and fast fluence, i.e. the two fields are not created. **This is the default choice in OFFBEAT if the user does not specify the `fastFlux` model in the `solverDict`**;
- [constant](../../classes/fastFlux/constantFastFlux.md) - the fast flux field is read from the <code>fastFlux</code> file in the starting time folder (the fast fluence evolves accordingly over time). **The old name for this class `fromLatestTime` is now deprecated**;
- [timeDependentAxialProfile](../../classes/fastFlux/timeDependentAxialProfile.md) - it prescribes a rod-average fast flux that changes over time with the possibility of adding a time-dependent axial profile.