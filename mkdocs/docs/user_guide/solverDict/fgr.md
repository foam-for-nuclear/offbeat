# **Fission Gas Release Model**

The <code><b>fissionGasRelease</b></code> class is used to handle the generation, diffusion and release of the gaseous fission products in OFFBEAT.

The fission gas release (FGR) model can be selected with the <code><b>fgr</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

## **Classes**

Currently OFFBEAT supports the following fgr models:

- [none](../../classes/fissionGasRelease/fissionGasRelease.md) - it neglects fission gas release (i.e. the gap gas composition remains as defined in the <c>0/gapGas</c> file). **This is the default choice in OFFBEAT if the user does not specify the `fgr` model in the `solverDict`**.
- [SCIANTIX](../../classes/fissionGasRelease/fgrSCIANTIX.md) - the 0-D open-source code [SCIANTIX](https://gitlab.com/poliminrg/sciantix) (developed at the PoliMi) is used as the local FGR module.
- [SCIANTIXRIA](../../classes/fissionGasRelease/fgrSCIANTIXRIA.md) - modification of the previous class, intended for use in RIA simulations. SCIANTIX is not actually called, but the SCIANTIX fields for grain concentration are read for initial condition of the transients. The fission gases are then released based on threshold (damage, burnup, temperature) defined by the user in the dictionary.