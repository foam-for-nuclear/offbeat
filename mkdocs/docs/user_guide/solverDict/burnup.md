# <b>Burnup Model</b>

The <code><b>burnup</b></code> class is used to enable the modeling of burnup evolution in OFFBEAT.  
The burnup field is by default named <code><b>Bu</b></code> and is in <code>MWd/MT<sub>oxide</sub></code>.

The burnup model can be selected with the <code><b>burnup</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code> dictionary, located in the <code><b>constant</b></code> folder).

## <b>Classes</b>

Currently, OFFBEAT supports the following burnup models:

- [none](../../classes/burnup/burnup.md) - neglects burnup evolution, the burnup field is not created. **This is the default choice in OFFBEAT if the user does not specify the `burnup` model in the `solverDict`**;
- [constant](../../classes/burnup/constantBurnup.md) - the burnup field is read from the <code>Bu</code> file in the starting time folder. **The old name for this class `fromLatestTime` is now deprecated**;
- [fromPower](../../classes/burnup/fromPower.md) - the burnup field is automatically derived from the local heat source;
- [Lassmann](../../classes/burnup/Lassmann.md) - employs a simplified depletion module that solves the Bateman equations for a small set of relevant nuclides with the objective of calculating the power density radial form factor.