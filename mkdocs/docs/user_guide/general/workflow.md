# **Workflow**

OFFBEAT operates similarly to typical solvers shipped with OpenFOAM (e.g., icoFoam, pisoFoam, etc.), where the user provides a mesh, control dictionary (`controlDict`), solution parameters (`fvSolution`), schemes dictionary (`fvSchemes`), an OFFBEAT-specific solver dictionary (`solverDict`), and initial and boundary conditions for the main fields in the initial time step folder (e.g., the folder `0/`).

This guide provides a brief list of the main steps and commands for OFFBEAT execution. Each part of the workflow will be elaborated upon in the following sections of this User's Guide. When executed sequentially from the case folder, these steps offer a comprehensive, step-by-step guide to the proper usage of the code.

---

## **List of Main Steps and Commands**

### 1. **Building the Mesh**

OpenFOAM offers extensive options for creating meshes, which can be generated in many formats and converted to OpenFOAM using various utilities. For 1D to 2D r-z simulations (but not necessarily for r-$\theta$), the meshes used for typical nuclear reactor fuels are relatively simple block-structured meshes, and OpenFOAM's built-in `blockMesh` tool can be used.

- **`blockMesh`**: This command builds the computational mesh based on the `blockMeshDict` dictionary located in the `system/` directory. Running this command creates the `constant/polyMesh/` folder, which stores all the information (points, faces, cells, etc.) about the generated mesh in several dictionaries.

	???+ note

		OFFBEAT comes with some custom tools to automatically generate `blockMeshDict` files for standard fuel rod geometrie. If you want to know more, check the [Tools](../../tools/index.md) part of this guide.

#### **1.a Changing Patch Type (Optional)**

Whether using `blockMesh` or a mesh generated from other software, the utility may not always know the details of boundary types (e.g., wedge, empty, coupled patches). In other cases the mesh might be provided by someone else and you may want to modify specific details of the patch or field specifications. In all these cases, the `changeDictionary` utility is helpful.

- **`changeDictionary`**: This command calls the respective OpenFOAM utility to modify files and dictionaries in your case folder. By default, it reads the file `system/changeDictionaryDict`. This command is frequently used in OFFBEAT test cases to change the entries for the `polyMesh/boundary` dictionary and can adjust patch definitions or other fields.

    ???+ warning

        While still extensively used in OFFBEAT `testCases`, the `changeDictionary` utility is deprecated in the latest version of OpenFOAM, where it has been replaced by the `foamDictionary` utility.  

---

### 2. **Setting Up the Simulation**

Once the mesh is ready, you can proceed to set up the simulation by configuring the following files:

- **`solverDict`**: The central dictionary specifying physics, models, and material properties.
- **Boundary and Initial Conditions**: These are defined in the `0/` folder (or another starting-time folder) and include fields such as temperature (`T`), displacement (`D`), and gas gap composition (`gapGas`).
- **`system/` settings**: Set up the `controlDict` (which manages start/end times, writing intervals, etc.), `fvSchemes` (discretization schemes), and `fvSolution` (numerical solver configurations and tolerances).

---

### 3. **Running the Code**

- **`offbeat`**: This command runs OFFBEAT. Information (or `log`) about the code execution will be displayed in the terminal window. If the `End` string is printed at the end of the execution, the run has successfully terminated. Always check for the presence of this string. To store the log information in a file, the following options are available:

    - **`offbeat | tee log.offbeat`**: This command pipes the output to both the terminal and a log file. Although any log file name can be chosen, using `log.offbeat` is suggested to ensure compatibility with other utilities or scripts.

    - **`offbeat | tee log.offbeat & gnuplot Residuals.gp`**: For convergence monitoring during the OFFBEAT iteration, this command runs the gnuplot script `Residuals.gp` alongside OFFBEAT, displaying an auto-refreshing gnuplot window to monitor residual behavior (if `Residuals.gp` is present in the case folder).

	- **`offbeat > log.offbeat`**: This command redirects the output to a log file without displaying it simultaneously on the terminal. This method stores the log information exclusively in the file.

---

### 4. **Post-Processing (Optional)**

Post-processing options in OpenFOAM are extensive. Most users utilize either [ParaView](https://www.paraview.org) or OpenFOAM's [built-in post-processing functionality](https://cfd.direct/openfoam/user-guide/v9-graphs-monitoring). See the [Data Processing](../data_processing/index.md) section for more details.

- **`paraFoam`**: This command enables the visualization of the fields computed by OFFBEAT and stored in the time-step folders via [ParaView](https://www.paraview.org). Only fields corresponding to written time steps can be visualized.

- **`postProcess -func "sampleDict"`**: This is an example of how to use the OpenFOAM `postProcess` utility to sample fields of interest in different points within the simulated domain. It reads from the `system/sampleDict` dictionary, and the results are stored in the `postProcessing/` folder. 

	???+ note 
	
		Only fields from written time steps can be sampled or probed in this way. Similar functionality can be added to the `functions` subdictionary in `controlDict` to extract data during the simulation. Refer to the post-processing section for further details.

- **`python plot.py`**: Simple Python scripts can be written to automatically plot quantities like the centerline temperature or local burnup. The example provided in the test case folder plots the centerline temperature at the center of the rod and the corresponding local burnup.

---

### 5. **Case Cleaning**

- **`foamListTimes -rm`**: This command deletes all stored time step directories from the case (except for the `0/` folder), erasing the results of a previous simulation. If available, the `Allclean` script is often used to remove not only the time folders but also any additional files created during the simulation, such as PNGs, logs, etc.