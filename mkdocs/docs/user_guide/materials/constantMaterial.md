# Constant material model {#constantMaterial}

The 'constant' material model (see constantMaterial.H) allows the user to set manually constant values for the thermomechanical properties of the material. If 'constant' material is selected, behavioral models (e.g. relocation, densification, swelling, etc.) are not considered. The model requires the user to define a value for each of the following properties:
	
* Density
* Specific Heat Capacity
* Thermal Conductivity
* Emissivity
* Young's Modulus
* Poisson's Ratio
* Shear Modulus
* Thermal Expansion Coefficient
* Reference Temperature

Further details on the use of the model are given in the header of the constantMaterial.H file. 
