# **Code Installation**

## **Requirements**
OFFBEAT has been developed and tested using Ubuntu and Red Hat Enterprise Linux distributions and should compile and run without problems on any Linux system supported by OpenFOAM. The code supports compilation on both the OpenFOAM Foundation version (.org) and the ESI version (.com) of OpenFOAM. Specifically, it is compatible with:

- [OpenFOAM-9.0](https://openfoam.org/release/9) from the OpenFOAM Foundation
- [OpenFOAM-v2312](https://www.openfoam.com/news/main-news/openfoam-v2312) from the ESI Group

The OFFBEAT compilation script automatically identifies which version of the OpenFOAM environment you have sourced.

---
 
## **Installation**
Ensure that OpenFOAM is installed and the environment properly configured for compiling OpenFOAM applications. See the [OpenFOAM User Guide](https://cfd.direct/openfoam/user-guide/v9-compiling-applications) for more information.

To clone the OFFBEAT repository and install the master branch version of the code, a folder is created to contain the code source files.
Typically this is inside the path OpenFOAM/userName-9/applications/solvers (where userName must be changed accordingly).
Then, the following commands are performed:

```sh
git clone https://gitlab.com/foam-for-nuclear/offbeat.git
cd offbeat
git checkout master
make
```

By default, OFFBEAT is compiled and installed in OpenFOAM user applications directory `$FOAM_USER_APPBIN`. OFFBEAT should be available from the commandline.
```sh
offbeat
```

If installed correctly, you should see the typical OpenFOAM splash screen for OFFBEAT.

```c
/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  9
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
Build  : <build_number>
Exec   : offbeat
Date   : <date>
Time   : 11:54:30
Host   : "pc10115"
PID    : 319486
I/O    : uncollated
Case   : <case_directory>
nProcs : 1
sigFpe : Enabling floating point exception trapping (FOAM_SIGFPE).
fileModificationChecking : Monitoring run-time modified files using timeStampMaster (fileModificationSkew 10)
allowSystemOperations : Allowing user-supplied system call operations

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
Create time
...

```