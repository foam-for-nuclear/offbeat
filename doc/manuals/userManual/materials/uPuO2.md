# (U,Pu)O2 material model {#upuo2}

Material model for (U,Pu)O2 fuel material (see UPUO2.H). The model uses default selections for thermomechanical properties and behavioral models, but different choices can be selected by the user. 

The default selections of the material model are listed in the table below.

Please refer to the header of each listed file for more information on the specific correlation/model.

### Thermomechanical properties

| Thermomechanical property | Default model                 |
|---------------------------|-------------------------------|
| Thermal Conductivity      | conductivityUPuO2.H           |
| Density                   | constantDensityUPuO2.H        |
| Emissivity                | emissivityRelapUO2.H          |
| Heat Capacity             | heatCapacityMatproUPuO2.H     |
| Poisson's Ratio           | constantPoissonRatioUPuO2.H   |
| Thermal Expansion         | thermalExpansionMatproUPuO2.H |
| Young's Modulus           | YoungModulusMatproUPuO2.H     |

### Behavioral models

In absence of suitable correlations, default behavioral models for (U,Pu)O2 correspond to the ones for UO2 fuel material.

| Behavioral phenomenon     | Default model                 |
|---------------------------|-------------------------------|
| Relocation                | relocationFRAPCON.H           |
| Densification             | densificationFRAPCON.H        |
| Swelling                  | swellingFRAPCON.H             |
| Failure                   | failureModel.H (i.e. "none")  |