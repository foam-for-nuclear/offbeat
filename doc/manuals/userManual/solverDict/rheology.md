# Rheology model {#rheology}

The <code><b>rheology</b></code> class is designed to handle the treatment of the material's rheological properties in OFFBEAT.

Also, with the use of simple switches it allows the user to activate/deactivate thermal strains, the plane stress approximation (useful for example for r-theta simulations) or the modified plane strain approximation (useful for 1.5D simulations). 

### Usage

The way the rheology laws are handled in the simulation <u>must</u> be selected with the <code><b>rheology</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 
Currently OFFBEAT supports only the following option:

- [byMaterial](@ref Foam.rheologyByMaterial), the specific constitutive law (e.g. elasticity, plasticity etc.) is selected at the material level i.e. by choosing the appropriate value for the keyword `rheologyModel` in each `materials` subdictionary.

The main constitutive laws (to be selected one for each material) currently available in OFFBEAT are:

- [elasticity](@ref Foam.elasticity), linear elasticity following Hooke's law is assumed. *Note that even if the constitutive law is linear-elastic, the total strain field might include additional components, such as thermal strain or swelling.*
- [misesPlasticity](@ref Foam.misesPlasticity), following Von Mises theory an additional (instantaneous) plastic strain component is calculated as a function of the stress state and of the cumulated plastic strain itself. Hooke's law is assumed to calculate the stress as a function of the elastic strain component.
- [misesPlasticCreep](@ref Foam.misesPlasticCreep), an additional time-dependent plastic strain component is calculated according to the specific creep model selected. Von Mises theory is used for the instantaneous portion of the plastic strain, while Hooke's law is assumed for calculating the stress as a function of the (remaining) elastic strain component.

**The previous models can be used both for linear and non-linear mechanical solvers (i.e. they are suitable both for the small strain approximation and for the finite-strain framework).**
The following ones instead should be used only in combination with large-strain mechanical solvers:

- [hyperElasticity](@ref Foam.hyperElasticity), the stress follows a StVenant-Kirchhoff hyperElasticity constitutive law.
- [hyperElasticMisesPlasticCreep](@ref Foam.hyperElasticMisesPlasticCreep), a time-dependent plastic strain component is calculated according to the specific creep model selected. The total strain is considered to be additive. The creep and additional strain component are removed from the total strain tensor, and the stress is calculated following a StVenant-Kirchhoff hyperElasticity constitutive law as a function of the (remaining) elastic strains.
- [neoHookeanElasticity](@ref Foam.neoHookeanElasticity), the stress follows a neoHookean elasticity constitutive law.
- [neoHookeanMisesPlasticity](@ref Foam.neoHookeanMisesPlasticity), Mises plasticity is used for the plastic component, while the stress follows a neoHookean elasticity constitutive law.


<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on the correct laws to use for large-strain simulations</b>
br><br>When modeling a fuel rod in large strain framework, it is typically still a good approximation to select the `misesPlasticCreep` law if one wants to include plastic phenomena in the simulation (e.g. for the cladding).
<br><br>Neo hookean or hyper elastic laws are strongly not suggested (they do not represent the behavior of fuel and cladding) and they are present in OFFBEAT mainly for benchmark and verification purposes.
</div>


#### Additional options

Additional options (not specific to single materials) might be selected in the `rheologyOptions` subdictionary. The main ones available in OFFBEAT are listed below:

- <b><c>thermalExpansion</c></b> - Activate or deactivate thermal strains. **It is on by default**.
- <b><c>solvePressureEqn</c></b>- If activated, an additional equation is solved for the hydrostatic stress (`sigmaHyd`) in order to quell or smooth some oscillations that can appear in the stress field, in particular in cases of considerable plastic strains. **It is off by default**
- <b><c>pressureSmoothingFactor</c></b> - Only valid if `solvePressureEqn` is activated, it sets the smoothing scale factor for the `sigmaHyd` equation. **It is 1.0 by default**.

<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note on the use of the pressure smoothing equation</b>
br><br>When activating the `solvePressureEqn` keyword an additional equation is solved for the field `sigmaHyd`. This means that a linear solver for this field should be added to the `fvSolution` file in the `system` folder (as it is typically done for `D` or `T`).
<br><br>Similarly a `sigmaHyd` file must be present in the initial folder (e.g. `0/` folder). The boundary condition for all non-empty or non-wedge patches should be of the type `extrapolatedFixedValue`.
<br><br>Finally, for standard fuel performance simulations it is not suggested to use the pressure smoothing equation option. If any oscillation arises in the stress field, it should be solved either by refining the mesh or by using the `multiMaterialCorrection` in the settings of `mechanicsSolver`.
</div>

Although designed for 2D and 3D simulations, OFFBEAT allows the user to run simplified simulations employing the plane-stress (for fuel r-theta dics) or the standard 1.5D approximation. In order to activate one of the two approximations, one can use the following keywords:

- <b><c>planeStress</c></b> - If activated, the strain tensor z-component is modified to consider the plane stress approximation. **Valid only for models with empty boundary conditions along the z-direction**. **It is off by default**. 
- <b><c>modifiedPlaneStrain</c></b> - If activated, the strain tensor z-component is modified to consider the typical modified-plane strain approximation of standard fuel performance. **Valid only for models with empty boundary conditions along the z-direction (and only if the `planeStress` switch is not activated**. **It is off by default**. 
- <b><c>precisionSpring</c></b> - Only valid if `modifiedPlaneStrain` is activated, it sets the convergence criterion for the modified plane strain loop. **Set to 1e-6 by default**.
- <b><c>springModulus</c></b> - Only valid if `modifiedPlaneStrain` is activated, spring modulus in N/m for the plenum spring. **Set to 1e-6 by default**. 
- <b><c>springPreCompression</c></b> - Only valid if `modifiedPlaneStrain` is activated, pre-compression in m for the plenum spring. **Set to 0 by default**. 
- <b><c>coolantPressureList</c></b> - The coolant pressure that acts on the top cladding cap. 

### Examples

Here is an example of the `solverDict` for a standard simulation without planeStrain or planeStress approximation:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>	
<pre style="margin: 0;"><code>
rheology byMaterial;

rheologyOptions
{
    // thermalExapansion is on by default
    thermalExpansion    on;

    // plane stress and plane strain are off by default
    planeStress         off;
    modifiedPlaneStrain off;

    // smoothing of pressure equation is off by default
    solvePressureEqn off;
}

// List of materials, one per cellZone.
materials
{
    fuel
    {
        material UO2;
        ...

        rheologyModel elasticity;
    }

    cladding
    {
        material zircaloy;
        ...

        rheologyModel misesPlasticity;

        ...
    }

    // ... sub-dictionaries for remaining materials, if any
}

</code></pre>
</div>  



Here instead is an example showing how one can set the `rheologyOptions` sub-dictionary inside the `solverDict` file to activate the modifiedPlaneStrain approximation (i.e. **for a 1.5D simulation**):

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheologyOptions
{
    // thermalExapansion is on by default
    thermalExpansion    on;

    // plane stress and plane strain approximation are off by default
    planeStress         off;
    modifiedPlaneStrain on;

    // In case of plane strain, the user might specify the following:
    // - convergence criterion for spring force balance  
    // - spring modulus
    // - pre-compression (initial compression, taken as positive in [m])
    // - the coolant pressure that acts on the top cladding cap
    precisionSpring      1e-6;
    springModulus        3.5e3;       
    springPreCompression 0.0;

    coolantPressureList
    {
        type                table;

        // Instead of "file", the user can insert "values"
        file                "$FOAM_CASE/constant/lists/topCladdingRingPressureCoolantList";

        // values
        // (
        //     (0.0 1e5)
        //     (100.0 1e6)
        // );

        format              foam;      // data format (optional)
        outOfBounds         clamp;     // optional out-of-bounds handling
        interpolationScheme linear;    // optional interpolation method
    }

    // smoothing of pressure equation is off by default
    solvePressureEqn off;
}

</code></pre>
</div>  

***

Return to [Setting the solverDict](@ref solverDict)