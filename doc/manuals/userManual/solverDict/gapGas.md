# Gap Gas Model {#gapGas}

The gap gas model type is selected with the `gapGas` keyword in the *solverDict* dictionary. The gap gas model handles the evolution of gap volume, pressure, temperature and composition. Currently OFFBEAT supports the following gap gas models:
- By selecting the mother class gapGasModel.H with the keyword `none`, no gap gas model is considered (not compatible with fgr or gap conductance models).
- gapFRAPCON.H , an extension to 3-D of a classical 1-D gap model. The calculation of the gap gas properties is mostly derived from FRAPCON. When selecting `FRAPCON` as gap gas model, the *gapGas* file stating the starting condition in terms of composition and pressure is required inside the starting time step folder (e.g. 0/).

NOTE: the volumes are in m3, and refer to the modeled geometry. For example, let us consider a rod plenum volume of 36 cm3. If the rod is reproduced with a 2-degree wedge mesh, the plenum volume in the model is:
\f[
    V_{model plenum} = \frac{2}{360}\cdot\frac{36}{1e6} m^3 = 2e-07 m^3    
\f]

***

Return to [User Manual](@ref userManual)
