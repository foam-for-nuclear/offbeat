# Fission Gas Release Model {#fgr}

The <code><b>fissionGasRelease</b></code> class is used to handle the generation, diffusion and release of the gaseous fission products in OFFBEAT.

### Usage
The fission gas release (FGR) model <u>must</u> be selected with the <code><b>fgr</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

Currently OFFBEAT supports the following fgr models:

- [none](@ref Foam.fissionGasRelease) - it neglects fission gas release (the gap gas composition remains as defined in the <c>0/gapGas</c> file.
- [SCIANTIX](@ref Foam.fgrSCIANTIX) - the 0-D open-source code [SCIANTIX](https://gitlab.com/poliminrg/sciantix) (developed at the PoliMi) is used as the local FGR module.

***

Return to [User Manual](@ref userManual)