# Case Folder Structure {#folderStructure}

This guide details the typical structure of an OFFBEAT case directory, crucial for organizing and running simulations effectively.

* <b><c>0/</c> folder</b>  (or the last time step folder if `latestTime` is selected as `startFrom` in `controlDict`). This is where you initialize your simulation. It includes initial and boundary conditions for main fields such as temperature (`T`), total displacement (`D` or `DD` for incremental mechanical subsolvers), and gas gap composition (`gapGas`).

* <b><c>constant/</c></b>. folder It contains:
   * The <b><c>polyMesh/</c></b> folder, with mesh connectivity (e.g. points, faces, etc). Note that the `polyMesh/` folder is automatically created by the mesh generation utility (e.g. blockMesh or ideasUnvToFoam or others).
   * The <b><c>solverDict</c></b> which is the central dictionary for OFFBEAT, specifying the physics/models included in the simulation as well as the material properties.
   
* <b><c>system/</c></b> folder. As a minimum, it contains the following files:
   * <b><c>controlDict</c></b>, provides the main parameters controlling the simulation. Start time, end time, adaptive time step options, writing options, run-time functionObjects to probe or register relevant quantities during the simulation etc. are all set within this dictionary. 
   * <b><c>fvSchemes</c></b>, indicates the settings for the discretization schemes used by the code for the various operators used by the governing equations, i.e. time derivatives, gradients, laplacians, etc.  
   * <b><c>fvSolution</c></b>, indicates the settings for the numerical solvers used by the code, i.e. type of linear solvers, tolerances, residual thresholds, number of correctors.

The <b><c>system/</c></b> folder may additionally contain any number of other files that are used by the other OpenFOAM tools or utilities during model pre- and post-processing. For instance, very common are:
   * <b><c>blockMeshDict</c></b>, provides instructions for creating the mesh using OpenFOAM's `blockMesh` utility. The mesh can also be created with any number of external tools like [Salome Plaform](https://www.salome-platform.org/), [Gmsh](https://gmsh.info/), bypassing `blockMesh`. OpenFOAM provides a number of mesh conversion utilities for this purpose, see the [OpenFOAM User Guide](https://cfd.direct/openfoam/user-guide/v9-mesh) for more details.
   * <b><c>changeDictionaryDict</c></b>, provides instruction to modify the patch boundaries or other dictionaries. Read by the `changeDictionary` utility.
   * <b><c>probes</c></b> or <b><c>sample</c></b>, files used by the OpenFOAM `postProcess` utility, see the [OpenFOAM User Guide](https://cfd.direct/openfoam/user-guide/v9-graphs-monitoring) for more details.
 
An OFFBEAT folder might contain the additional files (not necessary): 
* <b><c>input_settings.txt</c></b>, the input file for [SCIANTIX](@ref Foam.fgrSCIANTIX), the 0-D open-source code [SCIANTIX](https://gitlab.com/poliminrg/sciantix) (developed at the PoliMi) used as the FGR module in OFFBEAT (necessary only if `fgrSCIANTIX` is activated as fgr model in `solverDict`.
* <b><c>Allrun</c></b> and <b><c>Allclean</c></b> bash scripts or similar, used to sequentially perform a set of commands to respectively run and clean the case. 
* <b><c>Residuals.gp</c></b> or similar gnuplot script, used to visualize the main residuals of the simulation at runtime.
* <b><c>plot.py</c></b> or similar Python scripts, used as a simple tool to plot quantities typically recorded in the folder postProcessing/ (saved by OpenFOAM functionObjects such as probes, patchProbes or sample).

***

Return to [Case Folder Structure and Workflow ](@ref general)