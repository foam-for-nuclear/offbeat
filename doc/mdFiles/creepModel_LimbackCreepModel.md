### User documentation for `LimbackCreepModel` class {#creepModel_LimbackCreepModel}

#### Description
The `LimbackCreepModel` implements the computation of the creep strain rate for zircaloy cladding during normal operating conditions. This model includes both primary and secondary thermal creep and irradiation creep.

##### Secondary thermal creep:
The secondary thermal creep rate is given as:
\f[
    \dot{\varepsilon}^{th} = A \frac{E}{T} \left( \mathrm{sinh}\frac{a_i \sigma_{eff}}{E} \right) ^n \mathrm{exp} \left( -\frac{Q}{RT} \right) \newline
    a_i = A_0 (1 - A_1 [1 - \mathrm{exp}(-A_2 \phi^{A_3})]) \newline
    E = 1.148\times 10^5 - 59.9T
\f]
With T the temperature in Kelvin, \f$R=8.314\f$ (J/mol/K),\f$A_0=650\f$ (dimensionless), \f$A_1=0.56\f$ (dimensionless), \f$A_2=1.4\times 10^{-27}\f$ ((n/cm\f$^{2}\f$)\f$^{-A_3}\f$), and \f$A_3=1.3\f$ (dimensionless).


The values of A and Q depend of the cladding material:
\f[
    \begin{matrix}
        \textbf{Clad Type} & \textbf{A (K/MPa/Hr)} & \textbf{Q (kJ/mol)} & \textbf{n} \newline    
        \text{stress relief annealead (Zr2 or Zr4)} & 1.08\times 10^9 & 201 & 2.0 \newline
        \text{recrystallized annealed (Zr2 or M5)} & 5.47\times 10^8 & 198 & 3.5 \newline
        \text{partial recrystallized annealed (Zr2)} & 7.06\times 10^8 & 199 & 2.3 \newline
        \text{stress relief annealed (ZIRLO)} & 8.64\times 10^8 & 201 & f(\sigma_{eff})  
    \end{matrix}
\f]

\f$f(\sigma_eff)\f$ being given as a function of effective stress (Von Mises):
\f[
    \begin{matrix}
        2 & \text{if }\sigma_{eff} < 220 MPa \newline
        2.6 & \text{if }\sigma_{eff} < 400 MPa \newline
        1.2667 + 3.33\times 10^{-3} \sigma_{eff} & \text{if }\sigma_{eff} \geq 400 MPa
    \end{matrix}
\f]

##### Primary thermal creep :
The primary thermal creep rate is calculated as non-zero when secondary thermal creep occurs and the primary creep strain is below a saturation value. It is given as:
\f[
    \dot{\varepsilon}^p = \left( \varepsilon_{p-sat} \left( 1.0 - \mathrm{exp} \left[ -C \sqrt{\varepsilon_{secondary}(\tau_{primary} + \Delta t)} \right] \right) \right) \frac{1}{\Delta t}
\f]
Where c=52 (dimensionless) and \f$\tau_{primary}\f$ a time constant defined as:
\f[
    \tau_{primary} = \frac{1}{-C}\times \mathrm{log}\left(1.0 - \frac{\varepsilon^p_{old}}{\varepsilon_{p-sat}} \right)^2 \frac{1}{\dot{\varepsilon}_{secondary}}
\f]

The steady state creep rate \f$\dot{\varepsilon}_{secondary}\f$ being the sum of the secondary thermal creep rate and of the irradiation creep rate.
The primary satured strain value \f$\varepsilon_{p-sat}\f$ is given as:
\f[
    \varepsilon_{p-sat} = 0.0216\cdot\dot{\varepsilon}^{0.109}_{secondary} \times \left( 2 - \mathrm{tanh} \left( 35500\cdot\dot{\varepsilon}_{secondary} \right) ^{-2.05} \right)
\f]

##### Irradiation creep :
The irradiation creep rate is based on the model develloped by Hoppe. The irradiation creep rate is given as a function of current fast neutron flux and stress as :
\f[
    \dot{\varepsilon}^{irr} = C_0 \Phi^{C_1}\sigma_{eff}^{C_2}
\f]
with \f$\Phi\f$ the fast neutron flux in (n/m\f$^2\f$/s) and \f$\sigma_{eff}\f$ the effective (Von Mises) stress in Mpa.

The values of \f$C_0\f$,\f$C_1\f$ and \f$C_2\f$ depend on the clad material:
\f[
    \begin{matrix}
        \textbf{Clad Type} & C_0((n/m^s)^{C_1}MPa^{-C2}/hr) & C_1 & C_1 \newline    
        \text{stress relief annealead (Zr2 or Zr4)} & 3.557\times 10^{-24} & 0.85 & 1.0 \newline
        \text{recrystallized annealed (Zr2 or M5)} & 1.654\times 10^{-24} & 0.85 & 1.0 \newline
        \text{partial recrystallized annealed (Zr2)} & 2.714\times 10^{-24} & 0.85 & 1.0 \newline
        \text{stress relief annealed (ZIRLO)} & 2.846\times 10^{-24} & 0.85 & 1.0  
    \end{matrix}
\f]

##### Total creep :
Total creep rate is classicaly obtained by summation:
\f[
    \dot{\varepsilon}^{creep} = \dot{\varepsilon}^{primary} + \dot{\varepsilon}^{secondary} = \dot{\varepsilon}^{primary} + \dot{\varepsilon}^{th} + \dot{\varepsilon}^{irr} 
\f]

#### Usage
To use the `limbackCreepModel` creep model in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
...
materials
{
    material zircaloy;
    ...
    rheologyModel  misesPlasticCreep;
    rheologyModelOptions
    {
        yieldStressModel    constant;
        sigmaY 160e6;

        creepModel limbackCreep;
        cladType SRA; //RXA;//PRA;//ZIRLO;
    }
}
</code></pre>
</div>	

where for `cladType`:
- SRA corresponds to stress relief annealead (Zr2 or Zr4)
- RXA corresponds to recrystallized annealed (Zr2 or M5)
- PRA corresponds to partial recrystallized annealed (Zr2)
- ZIRLO corresponds to stress relief annealead ZIRLO

#### References
BISON documentation:
https://mooseframework.inl.gov/bison/source/materials/tensor_mechanics/ZryCreepLOCAUpdate.html

F. J. Erbacher, H. J. Neitzel, H. Rosinger, H. Schmidt, and K. Wiehr. Burst criterion of Zircaloy fuel claddings in a loss-of-coolant accident. In Zirconium in the Nuclear Industry, Fifth Conference, ASTM STP 754, D.G. Franklin Ed., 271–283. American Society for Testing and Materials, 1982.


