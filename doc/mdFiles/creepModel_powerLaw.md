### User documentation for `powerLaw` class {#creepModel_powerLaw}

#### Description
The `powerLaw` implements the computation of the creep strain rate in the following way:
\f[
    \dot{\varepsilon^{creep}}_{eff} = B \left( \frac{\sigma_eff}{\sigma_c} \right)^n
\f]
The following experimental constants need to be provided:
- \f$ B \f$ in [\f$ h^{-1} \f$]
- \f$ n \f$ in [-]
- \f$ \sigma_c \f$ in [MPa]

#### Usage
To use the `powerLaw` creep model in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    material zircaloy;
    ...
    rheologyModel  misesPlasticCreep;
    rheologyModelOptions
    {
        yieldStressModel    plasticStrainVsYieldStress;
        plasticStrainVsYieldStress table
        (
            (0    160e6)
        );
        creepModel powerLaw;
        B value;
        n value;
        sigmaC value;
    }
}

</code></pre>
</div>	