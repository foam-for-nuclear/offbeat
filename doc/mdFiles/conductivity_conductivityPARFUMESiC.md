### User documentation for `conductivityPARFUMESiC` class {#conductivity_conductivityPARFUMESiC}

Conductivity of PyC material from PARFUME manual.

#### Formulation

$$
\begin{aligned}
k = \frac{17885}{T} + 2.0
\end{aligned}
$$

where:

- \f$k\f$ is the inital thermal conductivity(W/m/K),
- \f$T\f$ is the temperature(T).



#### Usage

To use `conductivityPARFUMESiC` in OFFBEAT, you just need to specify the keyword **SiCPARFUME** for `conductivityModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        SiC
        {
            conductivityModel  SiCPARFUME;
        }

        ...

    }

</code></pre>
</div>
