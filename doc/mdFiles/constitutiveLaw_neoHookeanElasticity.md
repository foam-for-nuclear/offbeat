### User documentation for `neoHookeanElasticity` class {#constitutiveLaw_neoHookeanElasticity}

For general instructions on the modeling of the rheology of a material in OFFBEAT see [here](rheology.html).

The `neoHookeanElasticity` constitutive law class implements the neo-Hookean hyperelastic mechanical behavior.

As for others neo-Hookean or hyper-elastic behavior, it is mainly implemented in OFFBEAT for benchmark and verification purposes.

The Cauchy stress is given as:
\f[
      \sigma = 0.5\times K \times I \times \frac{(J^2 - 1)}{J} + \mu\times \frac{dev(J^{(-2.0/3.0)}\times B)}{J}
\f]
where:
- \\(K \\) is the bulk modulus
- \\(J \\) is the Jacobian of the deformation gradient
- \\(I \\) is the identity tensor
- \\(\mu \\) is the shear modulus
- \\(B \\) is the left Cauchy-Green deformation tensor = F & F.T()
- \\(F \\) is the deformation gradient

For more information, the interested user can refers to the solids4FOAM documentation.

#### Usage  

To use the `neoHookeanElasticity` constitutive law in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    cladding
    {
        material zircaloy;
        ...
        rheologyModel neoHookeanElasticity;
    }

}
</code></pre>
</div>	

### References
Simo & Hughes, Computational Inelasticity, 1998, Springer.


<!-- </details> -->
