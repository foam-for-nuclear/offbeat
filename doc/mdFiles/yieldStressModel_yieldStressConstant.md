### User documentation for `yieldStressConstant` class {#yieldStressModel_yieldStressConstant}

#### Description
The `yieldStressConstant` class is the mother class for all the types of yield stresses implemented in OFFBEAT.
It is intended to be used with the 'misesPlasticity' constitutive mechanical behaviour law.

#### Usage
To specify the yield stress as a constant, one must use the following syntyx in the solverDict:
</code></pre>
</div>	
<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
...
rheologyModel misesPlasticity;
rheologyModelOptions
{
    yieldStressModel constant;
    sigmaY 190e9;
}
</code></pre>
</div>	