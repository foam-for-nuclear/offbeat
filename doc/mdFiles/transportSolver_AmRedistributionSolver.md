### User documentation for `AmRedistribution` solver {#transportSolver_AmRedistributionSolver}

For general instructions on the modeling of element transport in OFFBEAT see [here](elementTransport.html). 

The `AmRedistribution` transport solver solves for the migration of Americium in MOX fuel pellets. 

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary> -->

#### Formulation

The redistribution process takes place due to two concurrent phenomena: thermal diffusion (i.e. Soret diffusion) and vapor transport, that are modelled through the following PDE: 

$$
\begin{aligned} 
\frac{\partial c}{\partial t} + \nabla \cdot \left\{\left[\nabla c + c(1-c)\frac{Q}{R\cdot T^2}\nabla T\right] - D\left[\frac{APcl}{d}\nabla T \cdot exp{\left(-\frac{D}{lv_p}\right)} \right]\right\} = 0
\end{aligned}
$$

where:

- \\( c \\)  is the Am concentration,
- \\( Q \\)  is the effective molar heat of transport,
- \\( R \\)  is the universal gas constant,
- \\( T \\)  is the temperature,
- \\( A \\)  is a model parameter,
- \\( P \\)  is the porosity,
- \\( v_p \\)  is the pore velocity,
- \\( D \\)  is the diffusion coefficient,
- \\( l \\)  is the pores' thickness,
- \\( d \\)  is the pores' diameter.

The diffusion coefficient for Am is calculated as:
$$
\begin{aligned} 
D_{Am} = 2.0\cdot10^{-5}exp{\left(\frac{-46324}{T}\right)}
\end{aligned}
$$

The pore migration contribution can be switched off with a dedicated keyword (see Usage below).

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `AmRedistribution` solver in OFFBEAT, the user will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	elementTransport byList;

	elementTransportOptions
	{
		solvers ( AmRedistribution );

		AmRedistributionOptions
		{
			poreMigration off;
		}
	}

</code></pre>
</div>

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> References </summary> -->

#### References

  - Valentino Di Marcello, Vincenzo Rondinella, Arndt Schubert, Jacques van de Laar, Paul Van Uffelen, Modelling actinide redistribution in mixed oxide fuel for sodium fast reactors, Progress in Nuclear Energy, Volume 72,2014, Pages 83-90,ISSN 0149-1970, https://doi.org/10.1016/j.pnucene.2013.10.008. 

  - Valentino Di Marcello, Arndt Schubert, Jacques van de Laar, Paul Van Uffelen, Extension of the TRANSURANUS plutonium redistribution model for fast reactor performance analysis, Nuclear Engineering and Design, Volume 248, 2012, Pages 149-155, ISSN 0029-5493, https://doi.org/10.1016/j.nucengdes.2012.03.037.
    
<!-- </details> -->