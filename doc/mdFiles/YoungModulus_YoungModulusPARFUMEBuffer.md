### User documentation for `YoungModulusPARFUMEBuffer` class {#YoungModulus_YoungModulusPARFUMEBuffer}

The `YoungModulusPARFUMEBuffer` model elastic modulus of Buffer material by the model taken from the code PARFUME.

#### Formulation

The elastic modulus of buffer is given by the correlation below:

$$
\begin{aligned}
E = 25.5(0.384 + 0.342\cdot10^{-3}\rho)(1.0 + 0.23\phi)\left[1.0 + 1.5\cdot10^{-4}(T-20)\right]
\end{aligned}
$$

where:

- \\(E \\) is elastic modulus in GPa,
- \\(\rho \\) is the density of buffer(kg/m³),
- \\(\phi \\) is fast neutron fluence(\\(10^{25}n/m^2 \\), E>0.18 MeV),
- \\(T \\) is the temperature of the buffer(\\(^\circ C \\)).



#### Usage

To use `YoungModulusPARFUMEBuffer` in OFFBEAT, you just need to specify the keyword **BufferPARFUME** for `YoungModulusModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with buffer material model
        Buffer
        {
            material buffer;
            YoungModulusModel       BufferPARFUME;
        }

        ...

    }

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |
|----------------|---------------------------------|------------------------|
|densityName     |Name used for the density field  |rho       |
|fastFluenceName  |Name of fast fluence field        |fastFluence |
