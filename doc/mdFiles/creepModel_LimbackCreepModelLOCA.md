### User documentation for `LimbackLOCACreepModel` class {#creepModel_LimbackCreepModelLOCA}

#### Description
The `LimbackLOCACreepModel` is a modified version of the [`limbackCreepModel`](creepModel_LimbackCreepModel.html). It adds a thermal creep behavior at high temperature (> 900 K).
Linear interpolation of the creep rate is assumed between the end of the standard `LimbackCreepModel` (700 K) and the begining of the LOCA thermal creep range.

Because the model parameters of the high temperature creep behavior were obtained from tension tests on Zircaloy-4, the only clad material considered for `LimbackCreepModel` when using the `LimbackLOCACreepModel` is the stress relief annealead zircaloy (Zr2 or Zr4). See the documentation of the [`limbackCreepModel`](creepModel_LimbackCreepModel.html) for more informations.

The high temperature thermal creep rate is given as:
\f[
    \dot{\varepsilon}^{th} = A\mathrm{exp} \left( -\frac{Q}{RT} \right) \sigma_{eff}^n
\f]

The model parameters depend on the zircaloy crystallographic phases:
\f[
    \begin{matrix}
        \textbf{Zircaloy phase present} & \textbf{Effective Creep Rate} (s^{-1}) & \textbf{A}(MPa^{-n}s^{-1}) & \textbf{Q (J/mol)} & \textbf{n} \newline    
        \text{pure }\alpha\text{ phase} &  \text{all creep rates} & 8737 &  3.21\times 10^{5} + 24.49(T-923.15) & 5.89 \newline
        \text{Mixed: 50% }\alpha\text{ phase, 50% }\beta \text{phase} & \dot{\varepsilon}_{eff} \le 3\times 10^{3}  & 0.24 & 1.02366\times 10^5 & 2.33 \newline
        \text{Mixed: 50% }\alpha\text{ phase, 50% }\beta \text{phase} & \dot{\varepsilon}_{eff} > 3\times 10^{3} & \text{linear interpolation of }\mathrm{ln}A & \text{linear interpolation} & \text{linear interpolation} \newline
        \text{pure }\beta\text{ phase} & \text{all creep rates} & 7.9 & 1.14919\times 10^5 & 3.78  
    \end{matrix}
\f]

In the mixed phase region, interpolations are performed to calculated the material parameters, depending on the strain rate.

For \f$\dot{\varepsilon}_{eff} \le 3\times 10^{-3} \mathrm{s}^{-1}\f$:
- linear interpolation of ln(A), Q and n are made between the values for pure \f$\alpha\f$ phase and the equally mixed phase of \f$\alpha + \beta\f$.
- and between \f$50\%\alpha + 50\%\beta\f$ and the pure \f$\beta\f$ phase.

For \f$\dot{\varepsilon}_{eff} > 3\times 10^{-3} \mathrm{s}^{-1}\f$, it is assumed that the values of ln(A), Q and n vary linearly between the \f$\alpha\f$ and \f$\beta\f$ phase.
#### Usage
To use the `LimbackLOCACreepModel` creep model in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
...
materials
{
    material zircaloy;
    ...
    rheologyModel  misesPlasticCreep;
    rheologyModelOptions
    {
        yieldStressModel    constant;
        sigmaY 160e6;

        creepModel limbackLOCA;

        // Activate primary creep (on by default)
        primaryCreep   off;
    
        // Activate irradiation creep (on by default)
        irradiationCreep   off;
    
        // Activate Newton-Raphson method for creep convergence
        // (off by default)
        NewtonRaphsonMethod   on;

        // Set inner convergence loop tolerance criterion in case 
        // Newton-Raphson method is activated (1e-6 by default)
        NewtonRaphsonTolerance   1e-6;
    }
}
</code></pre>
</div>	

#### References
BISON documentation:
https://mooseframework.inl.gov/bison/source/materials/tensor_mechanics/ZryCreepLOCAUpdate.html
