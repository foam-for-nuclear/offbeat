### User documentation for `neoHookeanMisesPlasticity` class {#constitutiveLaw_neoHookeanMisesPlasticity}

For general instructions on the modeling of the rheology of a material in OFFBEAT see [here](rheology.html).
For more informations on the neo-Hookean hyper-elastic behaviour see [here](constitutiveLaw_neoHookeanElasticity.md)
The `neoHookeanMisesPlasticity` constitutive law class implements the neo-Hookean hyper-elasto-plastic mechanical behavior.
The plastic behavior is the classical J2/Mises plasticity.

As for others neo-Hookean or hyper-elastic behavior, it is mainly implemented in OFFBEAT for benchmark and verification purposes.

For more information, the interested user can refers to the solids4FOAM documentation.

#### Usage  

To use the `neoHookeanMisesPlasticity` constitutive law in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    cladding
    {
        material zircaloy;
        ...
        rheologyModel neoHookeanMisesPlasticity;
        rheologyModelOptions
        {
            plasticStrainVsYieldStress table
            (
                (0.0 200e6)
                (0.005 250e6)
                (0.01 300e6)
            );
        }
    }

}
</code></pre>
</div>	

The evolution of the yield stress is declared by the user as a function of the equivalent plastic strain. For a constant yield stress, enter only one line in the table (for example : (0.0 200e6)).
### References
    Simo & Hughes, Computational Inelasticity, 1998, Springer.

    P. Cardiff, Z. Tukovic, P. De Jaeger, M. Clancy and A. Ivankovic. A
    Lagrangian cell-centred finite volume method for metal forming simulation,
    doi=10.1002/nme.5345.


<!-- </details> -->
