### User documentation for `constantCreepPrincipalStress` class {#creepModel_cconstantCreepPrincipalStress}

#### Description
The `correlationCreepPrincipalStress` implements the computation of the creep strain rate in the following way:
\f[
	\dot{\varepsilon}^{creep} = K\times[\sigma_1 - \nu_v\times(\sigma_2 + \sigma_3)]\times 
\f]
where:
- \\(\sigma_i) \\) are the principal stresses (egein values of the stress tensor)
- \\(\nu_c \\) is the Poisson ratio for creep
- \\(\K \\) is an user provided correlation of the creep coefficient (in Mpa \f$10^{25}\f$ n/m\f$^{2}\f$) of the form:
\f[
    K = A_0 + A_1\times T + A_2\times T^ {2}
\f]
with T the temperature in degree Celsius.

This class is used to verify the OFFBEAT results with IAEA CPR-6 benchmark for TRISO applications.

#### Usage
To use the `constantCreepPrincipalStress` creep model in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    "IPyC|OPyC"
    {
        material PyC;
        ...
        rheologyModel  misesPlasticCreep;
        rheologyModelOptions
        {
            yieldStressModel    plasticStrainVsYieldStress;
            plasticStrainVsYieldStress table
            (
                (0    160e6)
            );
            creepModel correlationCreepPrincipalStress;
            coefficientList            (4.386e-4 -9.70e-7 8.0294e-10);
            poissonRationForCreep       0.5;
            fluxConversionFactor       1.0;
            relax 1;
        }
    }
}
</code></pre>
</div>	