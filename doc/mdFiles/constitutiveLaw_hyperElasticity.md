### User documentation for `hyperElasticity` class {#constitutiveLaw_hyperElasticity}

For general instructions on the modeling of the rheology of a material in OFFBEAT see [here](rheology.html).

The `hyperElasticity` constitutive law class implements the StVenant-Kirchoff hyper-elastic behaviour.

\f[
    S = 2\mu \cdot E + \lambda \cdot tr(E)
\f]
with
\f[
        \sigma = \frac{1}{J}(F \cdot S \cdot F^{T})
\f]

where 
- \\(S \\) is the second Piola-Kirchoff stress tensor
- \\(\sigma \\) is the Cauchy stress tensor
- \\(\mu \\) and \\(\lambda \\) are the lamé parameters
- \\(E \\) is the large strain tensor
- \\(\varepsilon \\) is the small strain tensor
- \\(F \\) is the deformation gradient
- \\(J \\) is the determinant of the deformation gradient

As for others neo-Hookean or hyper-elastic behavior, it is mainly implemented in OFFBEAT for benchmark and verification purposes.

For more information, the interested user can refers to the solids4FOAM documentation.

#### Usage  

To use the `neoHookeanMisesPlasticity` constitutive law in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    fuel
    {
        material UO2;
        ...
        rheologyModel hyperElasticity;
        rheologyModelOptions
    }

}
</code></pre>
</div>	

### References
    Simo & Hughes, Computational Inelasticity, 1998, Springer.

    P. Cardiff, Z. Tukovic, P. De Jaeger, M. Clancy and A. Ivankovic. A
    Lagrangian cell-centred finite volume method for metal forming simulation,
    doi=10.1002/nme.5345.


<!-- </details> -->
