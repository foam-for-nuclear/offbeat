### User documentation for `constitutiveLaw` class {#constitutiveLaw_constitutiveLaw}

The `constitutiveLaw` class is the mother class for all the constitutive mechanical behavioral law implemented in OFFBEAT.
