### User documentation for `swellingPARFUMEPyC` class {#swelling_swellingPARFUMEPyC}

The `swellingPARFUMEPyC` model anisotropic irradiation-induced dimensional change of IPyC material by the model taken from the code PARFUME.

#### Formulation

The irradiation-induced eigenstrain of PyC is given by the correlation similair to that of buffer, but for 3 different components, as below:

$$
\varepsilon_i = a_{1,i}\phi + a_{2,i}\phi^2 + a_{3,i}\phi^3 + a_{4,i}\phi^4 \label{eq1}\tag{1} \\
$$

where:
- \\(i \\) can represent radial(r), tangential(t), isotropic(iso),
- \\(\varepsilon \\) is irradiation-induced eigenstrain (%),
- \\(\phi \\) is fast neutron fluence(\\(10^{25}n/m^2 \\), E>0.18 MeV),
- \\(a_1 \\) through \\(a_4 \\) for 3 components(r, t, iso) are temperature-dependent polynomial coefficients detailed in the CEGA report and in the PARFUME Theory and Model Basis Report (at a density of 1.96g/cm³).

Then an isotropic scaling factor \\(\mu \\) is calculated by the ratio of the isotropic strain at current density to the one at 1.96g/cm³. An anisotropic scaling factor \\(\nu \\) is calculated by the ratio of the difference between radial and tangential strain at current density to that at 1.96g/cm³.

$$
\begin{aligned}
\mu = \frac{\varepsilon_{iso}(\rho)}{\varepsilon_{iso}(\rho=1.96g/cm^3)}  
\end{aligned}
$$

$$
\begin{aligned}
\nu = \frac{\varepsilon_{r}(\rho)-\varepsilon_{t}(\rho)}{\varepsilon_{r}(\rho=1.96g/cm^3)-\varepsilon_{t}(\rho=1.96g/cm^3)}
\end{aligned}
$$

The above \\(\varepsilon_{iso}(\rho) \\), \\(\varepsilon_{iso}(\rho=1.96g/cm^3) \\), \\(\varepsilon_{r}(\rho) \\), \\(\varepsilon_{t}(\rho) \\), \\(\varepsilon_{r}(\rho=1.96g/cm^3) \\), \\(\varepsilon_{t}(\rho=1.96g/cm^3) \\) are the irradation eigenstrains at 1373 K and 3.7x10^25 n/m2 (E>0.18 MeV) and are taken from CEGA report and PARFUME Theory and Model Basis Report. The data are included in [swellingPARFUMEPyCdata.H](@ref Foam.swellingPARFUMEPyCdata)

Then the radial and tangential irradiation strains can be computes as below, with \\(\varepsilon_{iso}(1.96) \\), \\(\varepsilon_{r}(1.96) \\) and  \\(\varepsilon_{t}(1.96) \\) calculated at current BAF, temperature, fast neutron flux by ([1](#mjx-eqn-eq1)).

$$
\begin{aligned}
\varepsilon_r(\rho)=\mu\varepsilon_{iso}(1.96)+\nu\left[\varepsilon_r(1.96)-\varepsilon_{iso}(1.96)\right]
\end{aligned}
$$

$$
\begin{aligned}
\varepsilon_t(\rho)=\mu\varepsilon_{iso}(1.96)+\nu\left[\varepsilon_t(1.96)-\varepsilon_{iso}(1.96)\right]
\end{aligned}
$$

In 1-D case, the radial and tangential irradiation strain \\(\varepsilon_r \\) and \\(\varepsilon_t \\) are provided to \\(xx \\) component and \\(yy, zz \\) components respectively. However, in other cases, we need to convert the strain tensor in spherical coordinate to cartesian coordinate.

$$
\begin{aligned}
\mathbf{\varepsilon_{cart}}=\mathbf{R}\mathbf{\varepsilon_{sphe}}\mathbf{R^T}
\end{aligned}
$$


#### Usage

To use `swellingPARFUMEPyC` in OFFBEAT, you just need to specify the keyword **BufferPARFUME** for `swellingModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        "IPyC|OPyC"
        {
            material PyC;
            swellingModel       swellingPARFUMEPyC;
        }

        ...

    }

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |
|----------------|---------------------------------|------------------------|
|densityName     |Name used for the density field  |rho       |
|fastFluenceName  |Name of fast fluence field        |fastFluence |
|asFabricatedAnisotropy  |The as-fabricated anisotropy (dimensionless)  |1.0       |
|sphereCoordinate  |Whether to use spherical coordinate(in 1D case)        |true |
|fluxConversionFactor |Flux conversion factor. Conversion between flux(>0.1 MeV) to flux(>0.18 MeV) |1.0 |


#### TODO
- Add BAF as a field so it can vary with the fast neutron fluence.

####  References

Skerjanc, William F., & Demkowicz, Paul A. PARFUME Theory and Model Basis Report. United States. https://doi.org/10.2172/1471713
