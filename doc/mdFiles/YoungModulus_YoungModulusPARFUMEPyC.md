### User documentation for `YoungModulusPARFUMEPyC` class {#YoungModulus_YoungModulusPARFUMEPyC}

The `YoungModulusPARFUMEPyC` model elastic modulus of PyC material by the model taken from the code PARFUME.

#### Formulation

The elastic modulus of the PyC in the radial and tangential directions are given by the correlations below:

$$
\begin{aligned}
E_r = 25.5(0.384 + 0.324\rho)(1.463-0.463BAF_0)(2.985 - 0.0662L_c)(1 + 0.23\phi)(1 + 0.00015(T - 20)
\end{aligned}
$$

$$
\begin{aligned}
E_t = 25.5(0.384 + 0.324\rho)(0.481 + 0.519BAF_0)(2.985 - 0.0662L_c)(1 + 0.23\phi)(1 + 0.00015(T-20))
\end{aligned}
$$

where:
- \\(E_r \\) and \\(E_t \\) is radial and tangential directions in GPa,
- \\(\rho \\) is density(g/cm³),
- \\(BAF_0 \\) is the  initial degree of anisotropy,
- \\(\phi \\) is fast neutron fluence(\\(10^{25}n/m^2 \\), E>0.18 MeV),
- \\(L_c \\) is the crystallite diameter, the value is set to 30 Angstroms so that the 4th term equal to 1,
- \\(T \\) is the temperature(\\(^\circ C \\)).

OFFBEAT currently only supports the isotropic model, so the elastic modulus of PyC is computed as,

$$
\begin{aligned}
E = \frac{E_r + 2E_t}{3}
\end{aligned}
$$



#### Usage

To use `YoungModulusPARFUMEPyC` in OFFBEAT, you just need to specify the keyword **PyCPARFUME** for `YoungModulusModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        "IPyC|OPyC"
        {
            material PyC;
            YoungModulusModel       PyCPARFUME;
        }

        ...

    }

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |
|----------------|---------------------------------|------------------------|
|densityName     |Name used for the density field  |rho       |
|fastFluenceName  |Name of fast fluence field        |fastFluence |
|asFabricatedAnisotropy |The as-fabricated anisotropy (dimensionless) |1.0 |
|crystalliteDiameter |Crystallite diameter (Angstroms) |30.0 |
