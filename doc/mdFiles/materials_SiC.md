### User documentation for `SiC` class {#materials_SiC}

The `SiC`class in OFFBEAT allows you to select specific models for SiC material.

#### Usage

To use `SiC` in OFFBEAT, you just need to specify the keyword **SiC** for `material`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with SiC material model
        SiC
        {
            material SiC;
        }

        ...

    }

</code></pre>
</div>

The material property and behavioral models can selected with the specific keywords. Otherwise, default models will be used. All of the models are take from PARFUME theory manual.

  |                             | defaultModel                                              |  Options                                      |
  | ----------------------------| ----------------------------------------------------------| --------------------------------------------- |
  | densityModel                | [constant](@ref Foam.densityConstant)                     | [constant](@ref Foam.densityConstant) |
  | heatCapacityModel           | [SiCSnead](@ref Foam.heatCapacitySneadSiC)                | [constant](@ref Foam.heatCapacityConstant)<br />[SiCSnead](@ref Foam.heatCapacitySneadSiC) |
  | conductivityModel           | [SiCPARFUME](@ref Foam.conductivityPARFUMESiC)            | [constant](@ref Foam.conductivityConstant)<br />[SiCPARFUME](@ref Foam.conductivityPARFUMESiC) |
  | YoungModulusModel           | [SiCPARFUME](@ref Foam.YoungModulusPARFUMESiC)            | [constant](@ref Foam.YoungModulusConstant)<br />[SiCPARFUME](@ref Foam.YoungModulusPARFUMESiC)  |
  | PoissonRatioModel           | [constant](@ref Foam.PoissonRatioConstant)                | [constant](@ref Foam.PoissonRatioConstant) |
  | thermalExpansionModel       | [SiCPARFUME](@ref Foam.thermalExpansionPARFUMESiC)        | [constant](@ref Foam.thermalExpansionConstant)<br /> [SiCPARFUME](@ref Foam.thermalExpansionPARFUMESiC)<br />[SiCSnead](@ref Foam.thermalExpansionSneadSiC) |     
  | creepModel                  | | [MonolithicSiCCreepModel](@ref Foam.MonolithicSiCCreepModel)<br />|


#### Examples

Here is a example of the usage of 'SiC'

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em; overflow: scroll; height: 300px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with SiC material model
        SiC
        {
            material SiC;

            densityModel            constant;
            heatCapacityModel       SiCSnead;
            conductivityModel       SiCPARFUME;
            YoungModulusModel       SiCPARFUME;
            PoissonRatioModel       constant;
            thermalExpansionModel   SiCPARFUME;
            emissivityModel         constant;

            rho         rho     [1 -3 0 0 0]    3200.0;
            nu          nu      [0 0 0 0 0]     0.13;
            emissivity  emissivity       [0 0 0 0 0]   0.0;
            Tref        Tref    [ 0 0 0 1 0 ] 298.15;

            rheologyModel               elasticity;

        }

        ...

    }

</code></pre>
</div>
