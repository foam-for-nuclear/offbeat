### User documentation for `thermalExpansionPARFUMEBuffer` class {#thermalExpansion_thermalExpansionPARFUMEBuffer}

The `thermalExpansionPARFUMEBuffer` model thermal expansion strain of Buffer material by the model taken from the code PARFUME.

#### Formulation

The thermal expansion coefficient of buffer is given by the correlation below:

$$
\begin{aligned}
\alpha = 5(1 + 0.11\frac{T - 400}{700})
\end{aligned}
$$

where:

- \\(\alpha \\) is thermal expansion coefficient (\\(10^{-6}/K \\)),
- \\(T \\) is the temperature of the buffer(\\(^\circ C \\)).



#### Usage

To use `thermalExpansionPARFUMEBuffer` in OFFBEAT, you just need to specify the keyword **BufferPARFUME** for `thermalExpansionModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with buffer material model
        Buffer
        {
            material buffer;
            thermalExpansionModel       BufferPARFUME;
        }

        ...

    }

</code></pre>
</div>

#### Input parameters

There's no input parameter needed for `thermalExpansionPARFUMEBuffer`.
