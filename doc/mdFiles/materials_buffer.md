### User documentation for `buffer` class {#materials_buffer}

The `buffer`class in OFFBEAT allows you to select specific models for buffer material.

#### Usage

To use `buffer` in OFFBEAT, you just need to specify the keyword **buffer** for `material`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with buffer material model
        Buffer
        {
            material buffer;
        }

        ...

    }

</code></pre>
</div>

The material property and behavioral models can selected with the specific keywords. Otherwise, default models will be used. All of the models are take from PARFUME theory manual.

  |                             | defaultModel                                              |  Options                                      |
  | ----------------------------| ----------------------------------------------------------| --------------------------------------------- |
  | densityModel                | [constant](@ref Foam.densityConstant)                     | [constant](@ref Foam.densityConstant) |
  | heatCapacityModel           | [constant](@ref Foam.heatCapacityConstant)                | [constant](@ref Foam.heatCapacityConstant) |
  | conductivityModel           | [BufferPARFUME](@ref Foam.conductivityPARFUMEBuffer)      | [constant](@ref Foam.conductivityConstant)<br />[BufferPARFUME](@ref Foam.conductivityPARFUMEBuffer) |
  | YoungModulusModel           | [BufferPARFUME](@ref Foam.YoungModulusPARFUMEBuffer)      | [constant](@ref Foam.YoungModulusConstant)<br />[BufferPARFUME](@ref Foam.YoungModulusPARFUMEBuffer) |
  | PoissonRatioModel           | [constant](@ref Foam.PoissonRatioConstant)                | [constant](@ref Foam.PoissonRatioConstant) |
  | thermalExpansionModel       | [BufferPARFUME](@ref Foam.thermalExpansionPARFUMEBuffer)  | [constant](@ref Foam.thermalExpansionConstant)<br /> [BufferPARFUME](@ref Foam.thermalExpansionPARFUMEBuffer) |
  | swellingModel               | [BufferPARFUME](@ref Foam.swellingPARFUMEBuffer)          | [BufferPARFUME](@ref Foam.swellingPARFUMEBuffer)<br />[constant](@ref Foam.constantSwelling) |
  | creepModel                  | | [PARFUMUEBufferCreepModel](@ref Foam.PARFUMEBufferCreepModel)<br />[constantCreepPrincipalStress](@ref Foam.constantCreepPrincipalStress)<br />[correlationCreepPrincipalStress](@ref Foam.correlationCreepPrincipalStress)|     


#### Examples

Here is a example of the usage of 'buffer'.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em; overflow: scroll; height: 300px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with buffer material model
        Buffer
        {
            material buffer;

            densityModel            constant;
            heatCapacityModel       constant;
            conductivityModel       BufferPARFUME;
            YoungModulusModel       BufferPARFUME;
            PoissonRatioModel       constant;
            thermalExpansionModel   BufferPARFUME;
            swellingModel           BufferPARFUME;
            emissivityModel         constant;

            rho         rho     [1 -3 0 0 0]    1000.0;
            Cp          Cp      [0 2 -2 -1 0]   720.0;
            nu          nu      [0 0 0 0 0]     0.345;
            emissivity emissivity       [0 0 0 0 0]   0.0;
            Tref        Tref    [ 0 0 0 1 0 ] 298.15;

            rheologyModelOptions
            {
                plasticStrainVsYieldStress table
                (
                    (0    1e60)
                );
                relax 1;
                creepModel PARFUMUEBufferCreepModel;
                fluxConversionFactor       0.85;
            }

        }

        ...

    }

</code></pre>
</div>
