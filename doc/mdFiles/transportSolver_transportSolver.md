### User documentation for `transportSolver` class {#transportSolver_transportSolver}

The `transportSolver` class is the mother class for all the transport solvers implemented in OFFBEAT.

For general instructions on the modeling of element transport in OFFBEAT see [here](elementTransport.html).

