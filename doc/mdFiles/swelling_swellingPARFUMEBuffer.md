### User documentation for `swellingPARFUMEBuffer` class {#swelling_swellingPARFUMEBuffer}

The `swellingPARFUMEBuffer` model isotropic irradiation-induced dimensional change (usually is shrinkage) of Buffer material by the model taken from the code PARFUME.

#### Formulation

The irradiation-induced eigenstrain of buffer is given by the correlation below:

$$
\begin{aligned}
\varepsilon = a_1\phi + a_2\phi^2 + a_3\phi^3 + a_4\phi^4
\end{aligned}
$$

where:

- \\(\varepsilon \\) is irradiation-induced eigenstrain (%),
- \\(\phi \\) is fast neutron fluence(\\(10^{25}n/m^2 \\), E>0.18 MeV),
- \\(a_1 \\) through \\(a_4 \\) are temperature-dependent polynomial coefficients detailed in the CEGA report and in the PARFUME Theory and Model Basis Report (at a density of 1.96g/cm³).

Isotropic swelling strain computed using above equation is adjusted for other densities by applying a scaling factor \\(\mu \\), defined in the equation below:

$$
\begin{aligned}
\mu = \frac{\varepsilon(\rho)}{\varepsilon(\rho=1.96g/cm^3)}
\end{aligned}
$$

The scalaing factor is calculated by interpolation of the values in table below with the current \\(\rho \\) and \\(\rho=1.96 g/cm^3 \\). These data are the irradation eigenstrain of 1373 K and a fast fluence of 3.7x10^25 n/m2 (E>0.18 MeV).

|\\(\rho \\)(g/cm³) |1.0 |1.2 |1.4 |1.5 |1.6 |1.8 |1.9 |1.96 |2.0 |
|-------------------|----|----|----|----|----|----|----|-----|----|
|\\(\varepsilon \\)(%) |-16.15 |-13.11 |-9.98 |-8.93 |-6.97 |-4.42 |-3.41 |-2.75 |-2.33 |

#### Usage

To use `swellingPARFUMEBuffer` in OFFBEAT, you just need to specify the keyword **BufferPARFUME** for `swellingModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        //- Name of cellZone with buffer material model
        Buffer
        {
            material buffer;
            swellingModel       BufferPARFUME;
        }

        ...

    }

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |
|----------------|---------------------------------|------------------------|
|densityName     |Name used for the density field  |rho       |
|fastFluenceName  |Name of fast fluence field        |fastFluence |

####  References

Skerjanc, William F., & Demkowicz, Paul A. PARFUME Theory and Model Basis Report. United States. https://doi.org/10.2172/1471713
