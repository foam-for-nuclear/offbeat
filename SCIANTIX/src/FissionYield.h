//////////////////////////////////////////////////////
//                                                  //
//           S C I A N T I X                        //
//           ---------------                        //
//                                                  //
//  Version: 1.4                                    //
//  Year   : 2019                                   //
//  Authors: D. Pizzocri, T. Barani and L. Cognini  //
//                                                  //
//////////////////////////////////////////////////////

extern const double Fission_yield_Xe;
extern const double Fission_yield_Kr;
extern const double Fission_yield_He;
