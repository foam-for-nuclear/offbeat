///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include <cmath>
#include <vector>
#include "GlobalVariables.h"
#include "ErrorMessages.h"

double TrappingRate(double diffusion_coefficient, double bubble_radius, double bubble_density);
