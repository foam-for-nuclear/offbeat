///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include <string>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <fstream>

namespace ErrorMessages
{
  void Switch(std::string routine, std::string variable_name, int variable);
};
