///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 0.1                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include "GlobalVariables.h"
#include "InterGranularMicroCracking.h"
#include "InterGranularBubbleEvolution.h"
#include "InterGranularGasRelease.h"
#include "InterGranularGasSwelling.h"

void InterGranularGasBehavior( );
