///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include <cmath>
#include <vector>
#include "GlobalVariables.h"
#include "ErrorMessages.h"
#include "Solver.h"
#include "IntraGranularBubbleRadius.h"
#include "NucleationRate.h"
#include "ResolutionRate.h"

void IntraGranularBubbleEvolution( );
