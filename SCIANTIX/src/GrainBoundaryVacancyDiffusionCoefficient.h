///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 0.1                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include <cmath>
#include "GlobalVariables.h"
#include "ErrorMessages.h"

double GrainBoundaryVacancyDiffusionCoefficient(double temperature);
