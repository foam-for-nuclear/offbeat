# OFFBEAT

OpenFOAM Fuel BEhavior Analysis Tool (OFFBEAT) is a three-dimensional finite-volume nuclear fuel performance code based on the [OpenFOAM® C++ library](https://openfoam.org/). OFFBEAT is developed according to a cell-centered finite-volume framework solid mechanics. This is combined with a framework for thermal analysis and with numerical developments concerning the treatment of the gap heat transfer and contact, based on a mapping algorithm that allows the use of independent non-conformal meshes for fuel and cladding. The code considers the temperature and burnup dependence of the material properties, and it can model fuel densification, relocation, swelling, growth, fission gas release, creep, plasticity, and other relevant fuel behavior phenomena.
OFFBEAT is a joint development by the [Laboratory of Reactor Safety (LRS)](https://www.epfl.ch/labs/lrs) at École Polytechnique Fédérale de Lausanne (EPFL) and [Laboratory for Reactor Physics and Thermal-Hydraulics (LRT)](https://www.psi.ch/en/lrt) at the Paul Scherrer Institut (PSI).

## OpenFOAM version

The current version of OFFBEAT is compatible both with [OpenFOAM-9.0](https://openfoam.org/release/9) from the OpenFOAM Foundation and [OpenFOAM-v2312](https://www.openfoam.com/news/main-news/openfoam-v2312) from the ESI Group.

## Documentation

The main user guide and documentation for OFFBEAT can be found here: [OFFBEAT Documentation](https://foam-for-nuclear.gitlab.io/offbeat/).

For detailed API references and developer documentation, see: [Doxygen Documentation](https://foam-for-nuclear.gitlab.io/offbeat/doxygen).

## Forum

A forum to get support from the developers and the community is available at the following link : [Forum](https://foam-for-nuclear.org/phpBB/)